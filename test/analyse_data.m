clear all
garbage_size = 10;
%open the file and load data
fid = fopen('benchmark.out');
fscanf(fid, '%g,%g,%g,%g,', [4 garbage_size]);
data = fscanf(fid, '%g,%g,%g,%g,', [4 inf]);
fclose(fid);

%plot
figure(1);
subplot(2,2,1);
plot(data(1,:));
hold on
ypos = mean(data(1,:));
plot(get(gca,'xlim'), [ypos ypos], 'color', 'r');
title('Frame ms');
hold off

subplot(2,2,2);
plot(data(2,:));
hold on
ypos = mean(data(2,:));
plot(get(gca,'xlim'), [ypos ypos], 'color', 'r');
title('Process ms');
hold off

subplot(2,2,3);
plot(data(3,:));
hold on
ypos = mean(data(3,:));
plot(get(gca,'xlim'), [ypos ypos], 'color', 'r');
title('UI Process ms');
hold off

subplot(2,2,4);
plot(data(4,:));
hold on
ypos = mean(data(4,:));
plot(get(gca,'xlim'), [ypos ypos], 'color', 'r');
title('Draw ms');
hold off
