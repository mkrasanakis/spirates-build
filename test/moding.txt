Benchmarking
============
Benchmarking is enabled with the Insert button when in intro and is stopped with the Escape button.

Script
------
Benchmarking script should have the following formula:
loop.benchmark(true)
local loops = 120
local i=1
while i <= loops and loop.benchmark_signal() ~= BENCHMARK_SIGNAL.STOP do
	loop.refresh_frame();
	i = i + 1;
	--commands to benchmark
end
loop.benchmark(false)

Matlab
------
Data is analyzed with the appropriate Matlab file analyze_data.m (should have Matlab to run).
