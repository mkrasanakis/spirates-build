function wait(loops)
	local i = 1
	while(i <= loops)and(loop.benchmark_signal() ~= BENCHMARK_SIGNAL.STOP)do
		loop.refresh_frame()
		i = i + 1;
	end
end

loop.benchmark(true)
--initialize
print('BENCHAMARK: CREATION')
simulation.new_game("Demo", "Benchmark Player", 1)
simulation.speed(1)
print('BENCHAMARK: NOT MOVING')
wait(60)
--equip ship and start movement
print('BENCHAMARK: GAIN CONTOL OF SHIP')
ai.load_player()
ai.control_parent()
wait(60*4)
print('BENCHAMARK: CREATE DRIVETRAIN')
ai.load_ship()
ship.create_item("Propeller", 2)
print('BENCHAMARK: CREATE GENERATOR')
wait(60*2)
ship.create_item("Diesel Generator", 1)
wait(60)
print('BENCHAMARK: CREATE FUEL')
ship.create_item("Diesel", 0)
wait(60*2)
print('BENCHAMARK: MOVE')
ai.move_left()
wait(60*100)
loop.benchmark(false)
