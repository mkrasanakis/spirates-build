local t = {}

t.on_create = function()
	item.set_state(0)
end

t.on_burn = function(energy)
	if(math.floor(energy*10000)~=0)then
		if(item.get_state()==0)then
			item.add_emmiter("diesel_generator")
			local channel = item.get_attribute("channel")
			if(channel~=0)then
				sound.halt(channel-1)
			end
			channel = item.make_sound("data/items/generator/diesel_generator/running.ogg", -1, 0, 1)
			item.set_attribute("channel", channel+1)
		elseif(item.get_attribute("channel")==0)then
			local channel = item.make_sound("data/items/generator/diesel_generator/running.ogg", -1, 0, 1)
			item.set_attribute("channel", channel+1)
		end
		item.set_state(1)
		item.add_emmiter("diesel_generator")
	else
		if(item.get_state()==1)then
			item.remove_emmiter("diesel_generator")
			local channel = item.get_attribute("channel")
			if(channel~=0)then
				sound.halt(channel-1)
			end
			channel = item.make_sound("data/items/generator/diesel_generator/idle.ogg", -1, 0, 1)
			item.set_attribute("channel", channel+1)
		elseif(item.get_attribute("channel")==0)then
			local channel = item.make_sound("data/items/generator/diesel_generator/idle.ogg", -1, 0, 1)
			item.set_attribute("channel", channel+1)
		end
		item.set_state(0)
		item.remove_emmiter("diesel_generator")
	end
end

return t