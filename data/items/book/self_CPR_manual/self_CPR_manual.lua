local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.set_max_health(ai.get_max_health()+5*(1+item.get_rank()))
	end
end

return t