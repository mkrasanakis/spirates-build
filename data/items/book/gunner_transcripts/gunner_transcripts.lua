local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.set_ranged_combat(ai.get_ranged_combat()+1+item.get_rank())
	end
end

return t