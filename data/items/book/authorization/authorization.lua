local t = {}

t.on_use = function()
	if(object.is_character())then
		object.load_parent()
		local n = object.get_number_of_children()
		local i
		local faction = ai.get_faction()
		for i = 0, n-1 do
			object.load_child(i)
			if(ai.load_object())and(ai.get_faction()~=faction)then
				ai.add_relation(faction, 0.5)
			end
			object.load_parent()
		end
	end
end

return t