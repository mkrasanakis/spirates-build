local t = {}

t.on_use = function()
		description = ""
					.."\n<b>Chapter 2: Energy and Ships</b>\n"
					.."------------------------------------------------------------------------------------------\n"
					.."Most actions require ENERGY (the blue bar on the bottom of the screen).\n"
					.."\n"
					.."When in control of your CHARACTER, some actions can be used WITHOUT ENERGY (like running), but you take some DAMAGE.\n"
					.."\n"
					.."DRIVETRAINS (such as Propeller) provide propulsion by consuming energy\n"
					.."\n"
					.."ENGINES produce energy by burning FUEL\n"
					.."\n"
					.."CREW contribute towards the navigation requirement of the ship allowing it to move."
					.."But TOO MANY CREW hinders ship movement.\n"
	loop.reader(description)	
end

return t