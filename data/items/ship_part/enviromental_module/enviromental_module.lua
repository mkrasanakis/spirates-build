local t = {}

--on_process
t.on_process = function()
	if(object.is_ship())then
		ship.set_pressure_inside(1)
		ship.set_temperature_inside(300)
	end
end

return t