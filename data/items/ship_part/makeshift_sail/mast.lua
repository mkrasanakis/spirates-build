local t = {}

--on_process
t.on_process = function()
	if(object.is_in_water())or(object.is_in_air())then
		local val = math.abs(object.get_pressure()-1)*150
		if(val<0)then
			val = 0
		elseif(val>10)then
			val = 10
		else
			val = math.floor(val*100.0)/100.0
		end
		item.set_water_power(5+val)
	else
		item.set_water_power(10)
	end
end

t.on_place = t.on_process
t.on_update= t.on_process

return t