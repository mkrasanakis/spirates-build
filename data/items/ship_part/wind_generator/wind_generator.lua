local t = {}

--on_process
t.on_process = function()
	
	if(object.is_in_water())or(object.is_in_air())then
		local val = math.abs(object.get_pressure()-1)*5 + math.abs(object.get_horizontal_velocity())/100
		val = val*10
		if(val<1.0/500)then
			val = 0
		end
		item.set_generation(val)
		item.set_animation_speed(val)
	else
		item.set_generation(1)
		item.set_animation_speed(0)
	end
end

return t