local t = {}

t.on_use = function()
	if(object.is_character())then
		local x = object.get_x()
		local y = object.get_y()
		object.load_parent()
		object.create_character("", x, y, "Titan Exoskeleton")
	end
end

return t