local t = {}

t.on_use = function()
	if(object.is_character())then
		local r = object.get_r()
		local t = object.get_theta()
		object.load_parent()
		if(object.is_planet()==false)then
			r = object.get_r()
			t = object.get_theta()
			object.load_parent()
		end
		if(planet.load_object())then
			planet.create_ship(r, t, "Corvette")
		end
	end
end

return t