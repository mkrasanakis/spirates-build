local t = {}

t.on_use = function()
	if(object.is_character())then
		local x = object.get_x()
		local y = object.get_y()
		object.load_parent()
		object.create_building(x, y, "Computer")
	end
end

t.on_process = function()
	if(object.is_character())then
		ai.start_animation("right hand rise")
		ai.start_animation("left hand rise")
	end
end

return t