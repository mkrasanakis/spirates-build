local t = {}

t.on_use = function()
	if(object.is_character())then
		local x = math.floor(object.get_x()/8+0.5)*8
		local y = math.floor(object.get_y()/8)*8
		object.load_parent()
		object.create_building(x, y, "Ladder")
		object.create_building(x, y+8, "Ladder")
		object.create_building(x, y+16, "Ladder")
	end
end

t.on_process = function()
	if(object.is_character())then
		ai.start_item_animation("hand hold")
	end
end

return t