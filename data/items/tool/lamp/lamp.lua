local t = {}

--on_process
t.on_process = function()
	ai.start_item_animation("hand rise")
	ai.add_skill_bonus("Navigation", item.get_name(), 4, 1)
end

return t