local t = {}

--on_process
t.on_process = function()
	if(object.is_character())then
		local val = item.get_grade()+1
		if(val>3)then
			val = 2*val-3
		end
		ai.add_skill_bonus("Melee Combat", item.get_name(), val, 1)
		--ai.add_skill_bonus("Speed", item.get_name(), math.ceil(val/2), 1)
	end
end

return t