local t = {}

--on_place
t.on_place = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.create_limb_slot("left arm", "Icelander Armor Sleeve Arm")
		ai.create_limb_slot("right arm", "Icelander Armor Sleeve Arm")
		ai.create_limb_slot("left forearm", "Icelander Armor Sleeve Forearm")
		ai.create_limb_slot("right forearm", "Icelander Armor Sleeve Forearm")
		ai.create_limb_slot("left thigh", "Icelander Armor Sleeve Thigh")
		ai.create_limb_slot("right thigh", "Icelander Armor Sleeve Thigh")
		ai.create_limb_slot("left calf", "Icelander Armor Sleeve Calf")
		ai.create_limb_slot("right calf", "Icelander Armor Sleeve Calf")
	end
end
--on_remove
t.on_remove = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.remove_limb_slot("left arm")
		ai.remove_limb_slot("right arm")
		ai.remove_limb_slot("left forearm")
		ai.remove_limb_slot("right forearm")
		ai.remove_limb_slot("left thigh")
		ai.remove_limb_slot("right thigh")
		ai.remove_limb_slot("left calf")
		ai.remove_limb_slot("right calf")
	end
end

t.on_update = t.on_place
	
return t