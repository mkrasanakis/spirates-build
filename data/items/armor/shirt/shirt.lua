local t = {}

--on_process
t.on_process = function()
	if(object.is_character())then
		ai.add_skill_bonus("Endurance", item.get_name(), 5, 1)
	end
end
--on_place
t.on_place = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.create_limb_slot("left arm", "Shirt Sleeve")
		ai.create_limb_slot("right arm", "Shirt Sleeve")
	end
end
--on_remove
t.on_remove = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.remove_limb_slot("left arm")
		ai.remove_limb_slot("right arm")
	end
end

t.on_update = t.on_place

return t