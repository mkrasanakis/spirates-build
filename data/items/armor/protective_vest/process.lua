local t = {}

--on_place
t.on_place = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.create_limb_slot("left arm", "Protective Vest Sleeve Arm")
		ai.create_limb_slot("right arm", "Protective Vest Sleeve Arm")
		ai.create_limb_slot("left forearm", "Protective Vest Sleeve Forearm")
		ai.create_limb_slot("right forearm", "Protective Vest Sleeve Forearm")
		ai.create_limb_slot("left thigh", "Protective Vest Sleeve Thigh")
		ai.create_limb_slot("right thigh", "Protective Vest Sleeve Thigh")
		ai.create_limb_slot("left calf", "Protective Vest Sleeve Calf")
		ai.create_limb_slot("right calf", "Protective Vest Sleeve Calf")
	end
end
--on_remove
t.on_remove = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.remove_limb_slot("left arm")
		ai.remove_limb_slot("right arm")
		ai.remove_limb_slot("left forearm")
		ai.remove_limb_slot("right forearm")
		ai.remove_limb_slot("left thigh")
		ai.remove_limb_slot("right thigh")
		ai.remove_limb_slot("left calf")
		ai.remove_limb_slot("right calf")
	end
end

t.on_update = t.on_place
	
return t