local t = {}

--on_process
t.on_process = function()
	if(object.is_character())and(math.random()<0.2/60)then
		local faction = ai.get_faction()
		ai.set_bounty(faction, math.floor(ai.get_bounty(ai.get_faction())*0.6))
		if(ai.get_bounty(faction)<100)and(object.get_alias()~="Ted")then
			ai.set_bounty(faction, 0)
			object.set_alias("Ted")
			if(ai.is_player())then
				loop.message("Ted Suit: you are now just another Ted")
			end
		end
	end
end
--on_place
t.on_place = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.create_limb_slot("left arm", "Ted Suit Sleeve")
		ai.create_limb_slot("right arm", "Ted Suit Sleeve")
		ai.create_limb_slot("left forearm", "Ted Suit Sleeve Ending")
		ai.create_limb_slot("right forearm", "Ted Suit Sleeve Ending")
	end
end
--on_remove
t.on_remove = function()
	if(object.is_character())and(ai.get_item_limb()~="")then
		ai.remove_limb_slot("left arm")
		ai.remove_limb_slot("right arm")
		ai.remove_limb_slot("left forearm")
		ai.remove_limb_slot("right forearm")
	end
end

t.on_update = t.on_place

return t