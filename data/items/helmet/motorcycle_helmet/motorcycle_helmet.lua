local t = {}

--on_process
t.on_process = function()
	if(object.is_character())then
		ai.add_skill_bonus("Navigation", item.get_name(), 5, 1)
	end
end

t.on_place = t.on_process

return t