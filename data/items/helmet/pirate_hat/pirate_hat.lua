local t = {}

--on_process
t.on_process = function()
	if(object.is_character())then
		ai.add_skill_bonus("Endurance", item.get_name(), 1, 1)
		ai.add_skill_bonus("Speed", item.get_name(), 1, 1)
		ai.add_skill_bonus("Navigation", item.get_name(), 10, 1)
	end
end

t.on_place = t.on_process

return t