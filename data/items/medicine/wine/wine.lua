local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.add_air_loss_effect(-0.2, 40, item.get_name())
		ai.add_skill_bonus("Endurance", item.get_name(), 1+ai.get_skill_bonus("Speed", item.get_name()), 90, "Speed")
		ai.add_skill_bonus("Ranged Combat", "Drunk", -2+1.5*ai.get_skill_bonus("Ranged Combat", "Drunk"), 90, "Drunk")
		
		local wines_drunk = ai.get_attribute("total_wines_drunk")+1
		if(wines_drunk>=50)then
			wines_drunk = 0
			ai.add_skill_bonus("Max Health", item.get_name(), 5+ai.get_skill_bonus("Max Health", "Wine Longevity"), -1, "Wine Longevity")
			if(ai.is_player())then
				loop.message("Wine contributes to longevity: +5 Max Health")
			end
		end
		ai.set_attribute("total_wines_drunk", wines_drunk)
		
		local total_harmful_drinks = ai.get_attribute("total_harmful_drinks")+0.15--200 bottles of wine trigger alcoholic too!
		if(total_harmful_drinks>=30)then
			total_harmful_drinks = 0
			ai.add_skill_bonus("Max Health", item.get_name(), -5+ai.get_skill_bonus("Max Health", "Alcoholic"), -1, "Alcoholic")
			if(ai.is_player())then
				loop.message("Alcoholic: -5 Max Health")
			end
		end
		ai.set_attribute("total_harmful_drinks", total_harmful_drinks)
	end
end

return t