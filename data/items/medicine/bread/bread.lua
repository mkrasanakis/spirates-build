local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.add_damage_effect("Food", -1, 20, item.get_name())
		object.make_sound("data/items/medicine/bread/bread.ogg", 0, 0, 1)
		
		local total_cancer = ai.get_attribute("total_cancer")+1
		if(total_cancer>=100)then
			total_cancer = 0
			ai.add_skill_bonus("Max Health", item.get_name(), -30+ai.get_skill_bonus("Max Health", "Cancer"), -1, "Cancer")
			if(ai.is_player())then
				loop.message("Cancer (-30 Max Health)")
			end
		end
		ai.set_attribute("total_cancer", total_cancer)
	end
end

return t