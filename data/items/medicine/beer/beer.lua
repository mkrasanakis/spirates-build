local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.add_air_loss_effect(-0.3, 40, item.get_name())
		ai.add_skill_bonus("Endurance", "Drunk", -1+1.5*ai.get_skill_bonus("Endurance", "Drunk"), 90, "Drunk")
		ai.add_skill_bonus("Speed", "Drunk", -1+1.5*ai.get_skill_bonus("Speed", "Drunk"), 90, "Drunk")
		
		local total_harmful_drinks = ai.get_attribute("total_harmful_drinks")+1
		if(total_harmful_drinks>=30)then
			total_harmful_drinks = 0
			ai.add_skill_bonus("Max Health", item.get_name(), -5+ai.get_skill_bonus("Max Health", "Alcoholic"), -1, "Alcoholic")
			if(ai.is_player())then
				loop.message("Alcoholic: -5 Max Health")
			end
		end
		ai.set_attribute("total_harmful_drinks", total_harmful_drinks)
	end
end

return t