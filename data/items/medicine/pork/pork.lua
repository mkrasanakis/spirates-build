local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.add_damage_effect("Food", -2, 40, item.get_name())
		ai.add_skill_bonus("Max Health", item.get_name(), -3+ai.get_skill_bonus("Max Health", item.get_name()), 90)
		ai.add_skill_bonus("Endurance", item.get_name(), -1+ai.get_skill_bonus("Endurance", item.get_name()), 90)
		object.make_sound("data/items/medicine/pork/pork.ogg", 0, 0, 1)
	end
end

return t