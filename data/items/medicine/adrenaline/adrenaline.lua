local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		if(ai.skill_exists("Max Health"))then
			ai.add_skill_bonus("Max Health", "Adrenaline", -8*(1+item.get_grade())+ai.get_skill_bonus("Max Health", "Adrenaline"), 20*(1+item.get_grade()))
		end
		if(ai.skill_exists("Speed"))then
			ai.add_skill_bonus("Speed", "Adrenaline", 4*(1+item.get_grade())+ai.get_skill_bonus("Speed", "Adrenaline"), 20*(1+item.get_grade()))
		end
		if(ai.skill_exists("Melee Combat"))then
			ai.add_skill_bonus("Melee Combat", "Adrenaline", 4*(1+item.get_grade())+ai.get_skill_bonus("Melee Combat", "Adrenaline"), 20*(1+item.get_grade()))
		end
	end
end

return t