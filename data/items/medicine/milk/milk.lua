local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		--wine is equivalent to 5 drinks or 10 breads
		ai.add_damage_effect("Food", -1, 10, item.get_name())
		local total_harmful_drinks = ai.get_attribute("total_harmful_drinks")-5
		if(total_harmful_drinks<0)then
			total_harmful_drinks = 0
		end
		ai.set_attribute("total_harmful_drinks", total_harmful_drinks)
		local total_cancer = ai.get_attribute("total_cancer")-10
		if(total_cancer<0)then
			total_cancer = 0
		end
		ai.set_attribute("total_cancer", total_cancer)
		if(ai.is_player())then
			local sum = total_cancer+total_harmful_drinks*4
			if(sum>80)then
				loop.message("Feeling less ghastly")
			elseif(sum>40)then
				loop.message("Feeling better")
			elseif(sum>20)then
				loop.message("Feeling detoxified")
			elseif(sum>0)then
				loop.message("Feeling fresh")
			else
				loop.message("Feeling completely refreshed")
			end
		end
	end
end

return t