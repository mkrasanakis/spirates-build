local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.add_damage_effect("Food", -1, 20, item.get_name())
		ai.add_skill_bonus("Navigation", item.get_name(), 1+ai.get_skill_bonus("Navigation", item.get_name()), 40)
		ai.add_skill_bonus("Ranged Combat", item.get_name(), 1+ai.get_skill_bonus("Ranged Combat", item.get_name()), 40)
	end
end

return t