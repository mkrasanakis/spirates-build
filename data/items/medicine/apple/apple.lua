local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		ai.add_damage_effect("Food", -2, 8, item.get_name())
		object.make_sound("data/items/medicine/apple/apple.ogg", 0, 0, 1)
	end
end

return t