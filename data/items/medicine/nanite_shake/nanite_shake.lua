local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		if(ai.skill_exists("Max Health"))then
			ai.add_skill_bonus("Max Health", "Medicine Toxicity", -2*(1+item.get_grade())+ai.get_skill_bonus("Max Health", "Medicine Toxicity"), -1, "Medicine Toxicity")
			ai.add_skill_bonus("Max Health", "Nanite Shake", 20+10*item.get_grade()+ai.get_skill_bonus("Max Health", "Nanite Shake"), 40+20*item.get_grade())
		end
		ai.set_health(ai.get_health()+40+20*item.get_grade())
	end
end

return t