local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		if(ai.skill_exists("Speed"))then
			ai.add_skill_bonus("Speed", item.get_name(), -(2+item.get_grade())+ai.get_skill_bonus("Speed", "Antibiotics"), 20*(1+item.get_grade()*0.5))
		end
		if(ai.skill_exists("Melee Combat"))then
			ai.add_skill_bonus("Melee Combat", item.get_name(), -(2+item.get_grade())+ai.get_skill_bonus("Melee Combat", "Antibiotics"), 20*(1+item.get_grade()*0.5))
		end
		local dam
		if(item.get_grade()==0)then
			dam = 3
		else
			dam = 2+3*item.get_grade()
		end
		ai.add_damage_effect("Drugs",-dam, 20*(1+item.get_grade()*0.5),item.get_name())
	end
end

return t