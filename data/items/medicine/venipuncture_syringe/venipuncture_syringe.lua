local t = {}

--on_use
t.on_use = function()
	if(object.is_character())and(object.get_health()>7)then
		item.make_sound("data/items/medicine/venipuncture_syringe/splat.ogg", 0, 0, 1)
		object.add_health(-7)
		ai.create_inventory_item("Blood Capsule")
	end
end

return t