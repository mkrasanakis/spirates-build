local t = {}

--on_use
t.on_use = function()
	if(object.is_character())then
		if(ai.skill_exists("Max Health"))then
			ai.add_skill_bonus("Max Health", item.get_name(), 250, 240)
			ai.set_health(ai.get_max_health())
			ai.add_air(ai.get_max_health())
		end
	end
end

return t