local t = {}

--on_process
t.on_process = function()
	if(object.is_character())then
		local mappings = {}
		mappings["Melee Combat"] = "Melee Combat"
		mappings["Melee Combat AI"] = "Melee Combat"
		mappings["Ranged Combat"] = "Ranged Combat"
		mappings["Ranged Combat AI"] = "Ranged Combat"
		mappings["Navigation"] = "Navigation"
		mappings["Navigation AI"] = "Navigation"
		mappings["Speed"] = "Speed"
		mappings["Movement"] = "Speed"
		mappings["Movement AI"] = "Speed"
		mappings["Max Health"] = "Max Health"
		mappings["Material Durability"] = "Max Health"
		mappings["Endurance"] = "Endurance"
		mappings["Electricity"] = "Endurance"
		mappings["Electricity"] = "Electricity"
		
		for k,v in pairs(mappings) do
			if(item.get_attribute(v)~=0)and(ai.skill_exists(k))then
				ai.add_skill_bonus(k, item.get_name(), item.get_attribute(v), 1)
			end
		end
	end
end

return t