function apply_xml_node(effect_name, apply_to_all, chance, message_text, effect_category)
	push()
		local duration = xml.get_value("duration")
		if(duration)then
			duration = tonumber(duration)
		else
			duration = 600
		end
		local regeneration = xml.get_value("regeneration")
		if(regeneration)then
			regeneration = tonumber(regeneration)
		else
			regeneration = 0
		end
		local income = xml.get_value("income")
		if(income)then
			income = tonumber(income)
		else
			income = 0
		end
		local race = xml.get_value("race")
		if(race==nil)then
			race = ""
		end
		local mutation = xml.get_value("mutation")
		if(mutation==nil)then
			mutation = ""
		end
		local bonuses = {}
		local values = {}
		local number_of_bonuses = xml.count("bonus")
		for i=0,number_of_bonuses-1 do
			xml.push()
			xml.load_node("bonus", i)
			bonuses[i+1] = xml.get_value("name")
			values[i+1] = tonumber(xml.get_value("value"))
			xml.pop()
		end
		--create citizenship description
		local description = ""
		for i=1,#bonuses do
			if(description~="")then
				description = description.."<br/>"
			end
			if(values[i]>=0)then
				description = description.."+"
			end
			description = description..values[i].." "..bonuses[i];
		end
		description = message_text.."<br/><s>"..description.."</s>"

		--apply citizenship on characters
		local n = object.get_number_of_children()
		local faction = object.get_faction()
		for i = 0,n-1 do
			object.load_child(i)
			if(ai.load_object())then
				if((apply_to_all)or(ai.get_faction()==faction))and((race=="")or(ai.get_race()==race))and(math.random()<=chance)then
					local obtained = false
					for i=1,#bonuses do
						local inc_health = 0
						if(ai.skill_exists(bonuses[i]))then
							if(ai.get_skill_bonus(bonuses[i], effect_name)==0)then
								obtained = true
								if(bonuses[i]=="Max Health")then
									inc_health = values[i]
								end
							end
							ai.add_skill_bonus(bonuses[i], effect_name, values[i], duration, effect_category)
						end
						if(inc_health~=0)then
							object.add_health(inc_health)
						end
					end
					if(regeneration~=0)then
						ai.add_damage_effect("Regeneration", -regeneration, duration, effect_name)
					end
					if(obtained==false)and(mutation~="")then
						if(ai.is_player())then
							if(ai.get_attribute(effect_name)==4)then
								loop.message("<style rsize='1'>"..effect_name.." completed</style>")
							else
								loop.message("<style rsize='1'>"..effect_name.." progressed (stage "..ai.get_attribute(effect_name)..")</style>")
							end
						end
						if(ai.get_attribute(effect_name)==4)then
							ai.change_to(mutation)
							ai.set_attribute(effect_name, 0)
						else
							ai.set_attribute(effect_name, ai.get_attribute(effect_name)+1)
						end
					elseif(obtained)then
						ai.set_attribute(effect_name, 1)
						if(ai.is_player())then
							loop.message(description)
						end
					end
				end
			end
			object.load_parent()
		end
	pop()
end




--process prosperity
local prosperity = object.get_prosperity()
if(math.random()<0.01/60*(math.abs(prosperity)/2+1))then
	local change = (math.random()*2+2)
	if(math.random()*6<prosperity)then
		change = -change
	end
	object.add_prosperity(change)
	if(change<0)then
		--loop.message("The economy crashed in "..object.get_alias())
		loop.message("<style rsize='1'>Nearby economy crashed (lower prices)</style>")
	else
		--loop.message("The economy inflated in "..object.get_alias())
		loop.message("<style rsize='1'>Nearby economy inflated (higher prices)</style>")
	end
end

--process epidemics
if(math.random()<0.02/60)and(object.get_attribute("epidemic")==0)then
	local name = object.get_name()
	xml.load_node(name, "town", "name")
	local disease_count = xml.count("citizenship")
	if(disease_count>0)then
		local epidemic = math.random(1,disease_count)
		object.set_attribute("epidemic", epidemic)
		xml.load_node("disease", epidemic-1)
		--loop.message(xml.get_value("name").." epidemic has occurred in "..object.get_alias())
		loop.message("<style rsize='1'>"..xml.get_value("name").." epidemic</style>")
	end
end

--process diseases during epidemics
local epidemic = object.get_attribute("epidemic")
if(epidemic>0)and(math.random()<0.1)then
	local name = object.get_name()
	xml.load_node(name, "town", "name")
	local disease_name = ""
	if(xml.load_node("disease", epidemic-1))then
		disease_name = xml.get_value("name")
		apply_xml_node(disease_name, true, 0.2, "Caught"..disease_name, disease_name)
	end
	if(math.random()<0.2/60/4)then
		if(name~="")then
			--loop.message(disease_name.." epidemic has passed in "..object.get_alias())
			loop.message("<style rsize='1'>"..disease_name.." epidemic passed</style>")
		end
		object.set_attribute("epidemic", 0)
	end
end


--process citizenship
local citizenship = object.get_attribute("citizenship")-1
if(citizenship>=0)then
	local name = object.get_name()
	xml.load_node(name, "town", "name")
	if(xml.load_node("citizenship", citizenship))then
		apply_xml_node(object.get_alias(), false, 1, "Social Benefits", "Social Benefits")
	end	
end