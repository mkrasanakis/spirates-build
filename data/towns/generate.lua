--characteristics: general
local size_x = object.get_size_x()
local name = object.get_name()
xml.load_node(name, "town", "name")
--characteristics: faction
local faction_type = xml.get_value("faction_type")
local faction_name = xml.get_value("faction_name")
--characteristics: spawn options
local building_space = xml.get_value("building_space")
if(building_space)then
	building_space = tonumber(building_space)
else
	building_space = 30
end
local guards_per_building = xml.get_value("guards_per_building")
if(guards_per_building)then
	guards_per_building = tonumber(guards_per_building)
else
	guards_per_building = 0
end
local total_guards = xml.get_value("extra_guards")
if(total_guards)then
	total_guards = tonumber(total_guards)
else
	total_guards = 0
end
--characteristics: boxes
local box_min_distance = xml.get_value("box_min_distance")
if(box_min_distance)then
	box_min_distance = tonumber(box_min_distance)
else
	box_min_distance = 0
end
local box_max_pile_height = xml.get_value("box_max_pile_height")
if(box_max_pile_height)then
	box_max_pile_height = tonumber(box_max_pile_height)
else
	box_max_pile_height = 0
end
local box_max_pile_width = xml.get_value("box_max_pile_width")
if(box_max_pile_width)then
	box_max_pile_width = tonumber(box_max_pile_width)
else
	box_max_pile_width = 0
end
--characteristics: guards
local guard_types = {}
local guard_probabilities = {}
local number_of_guards = xml.count("guard")
local temp
for i=0,number_of_guards-1 do
	xml.push()
	xml.load_node("guard", i)
	guard_types[i+1] = xml.get_value("name")
	temp = xml.get_value("probability")
	if(temp==nil)then
		guard_probabilities[i+1] = 1
	else
		guard_probabilities[i+1] = tonumber(temp)
	end
	xml.pop()
end
--characteristics: buildings
local building_types = {}
local building_probabilities = {}
local ladder_probability = {}
local building_spawns = {}
local number_of_buildings = xml.count("building")
for i=0,number_of_buildings-1 do
	xml.push()
	xml.load_node("building", i)
	building_types[i+1] = xml.get_value("name")
	building_probabilities[i+1] = xml.get_value("probability")
	if(building_probabilities[i+1]==nil)then
		building_probabilities[i+1] = 1
	else
		building_probabilities[i+1] = tonumber(building_probabilities[i+1])
	end
	building_spawns[i+1] = xml.get_value("max_spawns")
	if(building_spawns[i+1]==nil)then
		building_spawns[i+1] = -1
	else
		building_spawns[i+1] = tonumber(building_spawns[i+1])
	end
	ladder_probability[i+1] = xml.get_value("ladder")
	if(ladder_probability[i+1]==nil)then
		ladder_probability[i+1] = 0
	else
		ladder_probability[i+1] = tonumber(ladder_probability[i+1])
	end
	xml.pop()
end
--characteristics: decorations
local decorations = {}
local number_of_decorations = xml.count("decoration")
for i=0,number_of_decorations-1 do
	xml.push()
	xml.load_node("decoration", i)
	decorations[i+1] = xml.get_value("name")
	xml.pop()
end
--select a random citizenship effect
local max_citizenship = xml.count("citizenship")
if(max_citizenship>0)then
	object.set_attribute("citizenship", math.random(1, max_citizenship))
end

--generation: faction
object.set_alias(ai.generate_name(faction_type))
if(faction_name==nil)then
	faction_name = object.get_alias()
end
ai.create_faction(faction_name, faction_type)
--generation: buildings
if(#building_types~=0)and(building_space~=0)then
	local edge_offset = 30
	local current_position = -size_x/2 + edge_offset
	while(current_position<size_x/2-edge_offset)do
		local building_added_probabilities = {}
		building_added_probabilities[1] = 0
		for i=1,#building_probabilities do
			if(building_spawns[i]~=0)then
				building_added_probabilities[i+1] = building_added_probabilities[i]+building_probabilities[i]
			else
				building_added_probabilities[i+1] = building_added_probabilities[i]
			end
		end
		for i=1,#building_added_probabilities do
			building_added_probabilities[i] = building_added_probabilities[i]/building_added_probabilities[#building_added_probabilities]
		end
		if(building_added_probabilities[#building_added_probabilities]==0)then
			break
		end
		--select a building
		local rnd = math.random()
		local choice = building_types[1]
		local ladder = ladder_probability[1]
		for i=1,#building_types do
			if(rnd<=building_added_probabilities[i+1])then
				choice = building_types[i]
				ladder = ladder_probability[i]
				building_spawns[i] = building_spawns[i]-1
				break
			end
		end
		--create building
		object.create_building(faction_name, current_position, 0, choice)
		--create ladder
		if(math.random()<=ladder)and(building.get_door_x()~=0)then
			push()
				building.load_self()
				local ladder_x = current_position - building.get_door_x()
				local target_height = object.get_size_y()
				local ladder_y = 0
			pop()
			while(ladder_y<target_height)do
				push()
				object.create_building(ladder_x, ladder_y, "Ladder")
				ladder_y = ladder_y+8
				pop()
			end
		end
		--move current position
		push()
			building.load_self()
			current_position = current_position + object.get_size_x() + math.random(building_space, 1.5*building_space)
		pop()
		--increase number of guards
		total_guards = total_guards + guards_per_building
	end
end
--generations: guards
if(#guard_types~=0)and(total_guards>0)then
	local guard_added_probabilities = {}
	guard_added_probabilities[1] = 0
	for i=1,#guard_probabilities do
		guard_added_probabilities[i+1] = guard_added_probabilities[i]+guard_probabilities[i]
	end
	while(total_guards>0)do
		--select a guard
		local rnd = math.random()*guard_added_probabilities[#guard_added_probabilities]
		local choice = "Failed to select guard"
		for i=1,#guard_types do
			if(rnd<=guard_added_probabilities[i+1])then
				choice = guard_types[i]
				break
			end
		end
		--spawn the guard
		local x = -size_x/2+math.random()*size_x*0.9
		object.create_character(faction_name, x, 0, choice)
		total_guards = total_guards-1
	end
end
--generations: decorations
if(#decorations==1)then
	object.create_building(faction_name, 0, 0, decorations[0])
elseif(#decorations>0)then
	for i=0,#decorations-1,1 do
		object.create_building(faction_name, -size_x*0.9/2+size_x*0.9*i/(#decorations-1), 0, decorations[i+1])
	end
end
--generations: boxes
if(box_min_distance~=0)then
	local x = -object.get_size_x()/2+16
	x = x + box_min_distance*math.random()*2
	while(x<object.get_size_x()/2-16)do
		local height = math.random(1,box_max_pile_height)
		local width = math.random(1,box_max_pile_width)
		while(2*height<width)do
			height = height+1
		end
		local i,j
		for i=1,height do
			for j=1,width do
				if((i-1)*width<=math.abs(j-1-width*0.5)*height)then
					object.create_building(x+j*8-4, i*8-4, "Box")
				end
			end
		end
		x = x + width*16
		x = x + box_min_distance + box_min_distance*math.random()*2
	end
end

