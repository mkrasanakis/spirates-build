--get first planet
local i
for i=object.get_number_of_children()-1,0,-1 do
	object.load_child(i)
	if(object.is_planet())then
		planet.load_object()
	end
	object.load_parent()
end
--create faction
local player_name = simulation.player_name()
local faction = "Crew of "..player_name
ai.create_faction(faction, "Pirates")
--find first town angle
local first_angle = 0
planet.load_self()
for i=object.get_number_of_children()-1,0,-1 do
	object.load_child(i)
	if(object.is_town())then
		first_angle = object.get_theta()
	end 
	object.load_parent()
	if(first_angle~=0)then
		break
	end
end
--create town at same coordinates
local initial_distance = 430
local i = (first_angle-initial_distance*360/3.14159/planet.get_r())/360*planet.get_table_sizes()
local w_r = planet.get_water_r(i)
planet.create_town(w_r+9, 360.0*(i-0.5)/planet.get_table_sizes(), 1, "Starting Town")
local factor = planet.get_table_sizes()/(2*3.141592*object.get_r())
local city_size = math.ceil(object.get_size_x()/2*factor)
object.set_size_x(city_size*2/factor+5)
for k=i-city_size,i+city_size do
	planet.set_bg_ground_r(object.get_r()+5+20*(1-math.cos((k-i)/planet.get_table_sizes()*3.141592)),k)
end
planet.clear_clouds(object.get_theta(), 400*360/3.14159/planet.get_r())
--local enemy_faction = object.get_faction())
ai.create_faction("Pirate Noobs", "Pirates")
object.create_character("Pirate Noobs", 0, 0, "Story Human Citizen Noob")
initial_distance = initial_distance-20
--create player, control ship and then switch to town
object.create_character(faction, -25, 0, "Story Human")
ai.control_self()
ai.set_as_player()
ai.load_self()
ai.create_limb_item('left hand', 'How to be a Pirate - Chapter 1')
ai.create_limb_item('right hand', 'Stainless Steel Sword (consumer)')
ai.create_limb_item('left alt', 'Bread')
ai.load_item('left alt')
item.add_quantity(5)
ai.create_limb_item('floor limb', 'UMMI Life Extender')
ai.create_limb_item('head', 'Pirate Hat')
object.set_alias(player_name)
ai.change_parent()
--create ship with first antagonist
planet.create_ship(planet.get_water_r(first_angle/360*planet.get_table_sizes()), first_angle-initial_distance*360/3.14159/planet.get_r(), "Raft")
ship.load_self()
ship.remove_item(2)
object.create_character("Pirate Noobs", 0, 0, "Story Human Melee Noob")
ai.create_limb_item('right hand', 'Stainless Steel Sword (consumer)')
initial_distance = initial_distance-70
--create a raft with second antagonist
planet.create_ship(planet.get_water_r(first_angle/360*planet.get_table_sizes()), first_angle-initial_distance*360/3.14159/planet.get_r(), "Raft")
ship.load_self()
object.create_character("Pirate Noobs", 0, 0, "Story Human Ranged Noob")
ai.create_limb_item('right hand', 'Pistol (consumer)')

dofile('data/stories/initial_relations.lua')
