local t={}

t.on_select = function()
	local faction = ""
	push()
	if(ai.load_speaker())then
		faction = ai.get_faction()
	end
	pop()
	if(faction~="")then
		ai.add_relation(faction, -5)
	end
	ai.trigger_combat()
	ai.change_to("Human")
end	

return t
