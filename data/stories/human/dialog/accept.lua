local t={}

t.on_select = function()
	local faction = ""
	push()
	if(ai.load_speaker())then
		faction = ai.get_faction()
		ai.create_inventory_item("How to be a Pirate - Chapter 3")
	end
	pop()
	if(faction~="")then
		ai.set_faction(faction)
		ai.change_to("Human")
	end
end	

return t
