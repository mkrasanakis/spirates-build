ai.load_player()
local factions = {ai.get_faction(), "Pirates", "Feline", "Rebels"}

for i=1,#factions do
	for j=i+1,#factions do
		if(factions[i]~=factions[j])then
			local val = -4*(math.random()*math.random())+4*(math.random()*math.random())
			if(val<0)then
				val = 0
			end
			ai.set_relations(factions[i], factions[j], val)
			ai.set_relations(factions[j], factions[i], val)
		end
	end
end