--get first planet
local i
for i=0,object.get_number_of_children()-1 do
	object.load_child(i)
	if(object.is_planet() and object.is_satellite()==false)then
		planet.load_object()
	end
	object.load_parent()
end

--create demo character
planet.create_ship(planet.get_water_r(0)+50, 0, "Yatch")
ship.load_self()
ai.create_faction("Demo Faction", "Pirates")
object.create_character("Demo Faction", ship.get_control_x(), ship.get_control_y(), "Story Human")
ai.control_self()
ai.set_as_player()
