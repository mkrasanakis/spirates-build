All stories must have the following nodes:
* name
* id: must be unique between all stories)
* description
* portrait: path to appropriate file

They can also have the following nodes:
*space: a <space> node that contains star systems and appropriate probabilities.
        most stories will find the one in space.xml convenient.
        if not defined, space is not created in the game
*system: the starting star system inside which the game will start.
         if space is created, the starting system is placed in its center
*initialization: a path to a LUA script that usually creates the player in the starting star system