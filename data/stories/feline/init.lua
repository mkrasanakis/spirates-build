--get first planet
local i
for i=object.get_number_of_children()-1,0,-1 do
	object.load_child(i)
	if(object.is_planet())then
		planet.load_object()
	end
	object.load_parent()
end
--create faction
local player_name = simulation.player_name()
local faction = "Crew of "..player_name
ai.create_faction(faction, "Feline")
--create ship
planet.create_ship(planet.get_water_r(0)+50, 0, "Raft")
ship.load_self()
--create player and control ship
object.create_character(faction, ship.get_control_x(), ship.get_control_y(), "Story Feline")
ai.own_parent()
ai.control_self()
if(ship.load_object())then
	ship.update_crew()
end
ai.set_as_player()
ai.load_self()
object.set_alias(player_name)

dofile('data/stories/initial_relations.lua')