object.create_star(0, 0, 0, 0, "Sun")
global_current_planet = 1
global_max_planet = 1
object.create_planet(400000, 0, 0, 0, "Demo")
--create planet orbit
push()
planet.load_self()
local planet_id = object.id()
object.load_parent()
for i=0,object.get_number_of_children()-1 do
	object.load_child(i)
	if(object.is_star())then
		planet.orbit(2400)
	end
	object.load_parent()
end
pop()
--create moon
object.create_planet(450000, 0, 0, 0, "Moon")
--moon orbits planet
push()
object.load_by_id(planet_id)
planet.orbit(24)
pop()


for i=1, 18 do
	local r = 450000 + math.random(150000, 250000)
	local t = math.random()*2*3.14159
	local x = math.cos(t)*r
	local y = math.sin(t)*r
	local speed_x = y/200
	local speed_y = -x/200
	object.create_dropped_item(x, y, speed_x, speed_y, "Meteorite")
end