local x = (math.random()+1)*1500000/5
local y = (math.random()+1)*1500000/5
local speed_x = math.random()*5000-10000
local speed_y = math.random()*5000-10000
local x_center = (math.random()-0.5)*1500000/2
local y_center = (math.random()-0.5)*1500000/2 ---math.random()*200000-400000

local stars = {"Sun", "Neutron Star", "Red Giant", "Massive Star"}
local star1 = stars[math.random(1, #stars)]
local star2 = stars[math.random(1, #stars)]

object.create_star(x+x_center, y+y_center, speed_x, speed_y, star1)
object.create_star(-x+x_center, -y+y_center, -speed_x, -speed_y, star2)