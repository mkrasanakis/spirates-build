--make asteroids
local asteroid_count = 0
local sel = math.random()
if(sel<0.05)then
	asteroid_count = 50
elseif(sel<0.3)then
	asteroid_count = 30
elseif(sel<0.8)then
	asteroid_count = 18
end
asteroid_count = math.random(asteroid_count, math.ceil(asteroid_count*1.4))
for i= 1, asteroid_count do
	local r = 250000 + 3*math.random(150000, 250000)
	local t = math.random()*2*3.14159
	local x = math.cos(t)*r
	local y = math.sin(t)*r
	local speed_x = y/200
	local speed_y = -x/200
	if(math.random()<0.5)then
		speed_y = -speed_y
	end
	if(math.random()<0.2)then
		object.create_dropped_item(x, y, speed_x, speed_y, "Frozen Meteorite")
	elseif(math.random()<0.1)then
		object.create_dropped_item(x, y, speed_x, speed_y, "Pallite Meteorite")
	else
		object.create_dropped_item(x, y, speed_x, speed_y, "Meteorite")
	end
end