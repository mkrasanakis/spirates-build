local x_center = (math.random()-0.5)*1500000/2
local y_center = (math.random()-0.5)*1500000/2 ---math.random()*200000-400000

local stars = {"Sun", "Neutron Star", "Red Giant", "Massive Star"}
local star = stars[math.random(1, #stars)]

object.create_star(x_center, y_center, 0, 0, star)