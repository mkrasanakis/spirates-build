local number_of_planets = math.random(1, 4)

local x_center = 0 --math.random()*2000000-4000000
local y_center = 0 --math.random()*2000000-4000000

local least_dist = 250000

global_current_planet = 0
global_max_planet = number_of_planets

for i=1, number_of_planets do
	global_current_planet = i
	--create position of planet
	local t = math.random()*2*3.14159
	local r = least_dist + math.random(150000, 250000)
	least_dist = r
	local x = math.cos(t)*r
	local y = math.sin(t)*r
	--create speed of planet
	local speed_x = y/100
	local speed_y = -x/100
	
	local planets = {"Terrestrial", "Gas Giant", "Frozen Planet", "Desert Planet"}
	local orbits  = {"Moon", "Moon", "Clockwork Planet"}
	local selection = planets[math.random(1,#planets)]
	object.create_planet(x+x_center, y+y_center, speed_x, speed_y, selection)
	--set planet on orbit
	push()
	planet.load_self()
	local planet_id = object.id()
	object.load_parent()
	for i=0,object.get_number_of_children()-1 do
		object.load_child(i)
		if(object.is_star())then
			planet.orbit(2400*(1+math.random()))
		end
		object.load_parent()
	end
	pop()
	--create moon
	if((math.random()<0.5)and(selection=="Terrestrial"))or((math.random()<0.25)and(selection=="Desert Planet"))or((math.random()<0.25)and(selection=="Gas Giant"))then
		local tm = math.random(2*3.14159)
		object.create_planet(x+math.cos(tm)*50000, y+math.sin(tm)*50000, 0, 0, orbits[math.random(1,#orbits)])
		--moon orbits planet
		push()
		object.load_by_id(planet_id)
		planet.orbit(24)
		pop()
	end
end


--make asteroids
local asteroid_count = 0
local sel = math.random()
if(sel<0.05)then
	asteroid_count = 50
elseif(sel<0.3)then
	asteroid_count = 30
elseif(sel<0.8)then
	asteroid_count = 18
end
asteroid_count = math.random(asteroid_count, math.ceil(asteroid_count*1.4))
for i= 1, asteroid_count do
	local r = 250000 + 3*math.random(150000, 250000)
	local t = math.random()*2*3.14159
	local x = math.cos(t)*r
	local y = math.sin(t)*r
	local speed_x = y/200
	local speed_y = -x/200
	if(math.random()<0.5)then
		speed_y = -speed_y
	end
	if(math.random()<0.2)then
		object.create_dropped_item(x, y, speed_x, speed_y, "Frozen Meteorite")
	elseif(math.random()<0.1)then
		object.create_dropped_item(x, y, speed_x, speed_y, "Pallite Meteorite")
	else
		object.create_dropped_item(x, y, speed_x, speed_y, "Meteorite")
	end
end