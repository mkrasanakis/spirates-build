local save_dir = xml.get_value("save","file","name","path")
local lfs = require"lfs"

function string.ends(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

function table.reverse ( tab )
    local size = #tab
    local newTable = {}
 
    for i,v in ipairs ( tab ) do
        newTable[size-i] = v
    end
 
    return newTable
end


function loaddir (path)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local f = path..'/'..file
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode == "directory" then
                loaddir (f)
            else
                local name = file
                if string.ends(name,'.save') then
                    files[#files+1] = {name=f,time=attr.modification}
                end
            end
        end
    end
end

function sort_by_time(f1,f2)
	return f1.time > f2.time
end

function add_file(f)
	loop.add_saved_game(f.name)
end


files = {}
loaddir(save_dir);
table.sort(files,sort_by_time)
for k,v in ipairs(files) do add_file(v) end
