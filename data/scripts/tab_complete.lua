function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function trim(s)
  return s:match'^%s*(.*%S)' or ''
end

function escape_for_pattern(current_comp)
	current_comp = current_comp:gsub("%(","%%(")
	current_comp = current_comp:gsub("%)","%%)")
	return current_comp
end

local a = trim(...)
local split_p = a:find("([\"\'][^\"\']*)$")
if split_p then
	local keep = a:sub(1,split_p)
	local quote_type = a:sub(split_p,split_p)
	local process = a:sub(split_p+1)
	local nquotes = select(2, string.gsub(a, "[" .. quote_type .. "]", ""))
	if nquotes % 2 == 1 then
		local current_comp = escape_for_pattern(process:match('[^\\/]*$'))
		
		local directory = process:match('^.*[/\\]') or ''
		local prefix = lfs.currentdir() .. '/'
		if process[1] == "/" or process:find(':') then
			prefix = ''
		end
		local final_dir = prefix .. directory
		print(directory)
		
		local t = {}
		for f in lfs.dir(final_dir) do
			if f:sub(1,1) ~= '.' and f:match('^'..current_comp) then
				local str = keep..directory..f;
				local attr = lfs.attributes(directory..f)
				if attr.mode == "directory" then 
					str = str .. '/'
				else
					str = str .. quote_type
				end
				table.insert(t,str) 
			end
		end
		
		if #t == 0 then
			if xml_names_list == nil then
				xml_names_list = xml.list("")
			end
			for i, x in ipairs(xml_names_list) do
				if x:match('^' .. current_comp) then
					local str = keep .. x .. quote_type
					table.insert(t,str) 
				end
			end
		end
		return unpack(t)
	end
end

local split_p = a:find("%w[%w._]+$")
if split_p then
	local keep = a:sub(1,split_p-1)
	local process = a:sub(split_p)

	split_s,split_e = process:find('[^\.]-$')
	local current_comp = process:sub(split_s,split_e)
	local namespace_name = ""
	if split_s >= 3 then
		namespace_name = process:sub(1,split_s-2) or ''
	end

	--print('comp='..current_comp.."  table="..namespace_name)
	namespace_table = _G;
	if #namespace_name > 0 then
		namespace_name = namespace_name:gsub("%.+$","")
		local str = "namespace_table = ".. namespace_name
		ret = loadstring(str)
		if ret and ret() and type(namespace_table) ~= 'table' then return a end
	end

	local t = {}
	for k,v in pairs(namespace_table) do
		if k:sub(1,1) ~= '_' and k:match('^'..current_comp) then
			local str = keep..namespace_name
			if #namespace_name > 0 then
				str = str .. '.'
			end
			str = str .. k;
			if type(v) == "function" then
				str = str .. '('
			elseif type(v) == "table" then
				str = str .. '.'
			end
			table.insert(t,str) 
		end
	end     
	return unpack(t);
end

return a