local lfs = require('lfs');

local filenames = {}
function string.ends(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

function loaddir (path)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local f = path..'/'..file
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode == "directory" then
                loaddir (f)
            else
                local name = file
                if string.ends(name,'.xml') then
                    filenames[#filenames+1] = f;
                end
            end
        end
    end
end

local data_dir = xml.get_value("data","file","name","path")
print("Assesing files to load")
loaddir (data_dir)
print("Found "..#filenames.." files.")

local progress = 0
for i,f in ipairs(filenames) do
	message = "Loading "..f;
	if f:ends('.xml') then
		xml.load_file(f)
	elseif f:ends('.png') then
		assets.load_image(f)
	end
	progress = progress + 1/(#filenames+1)
	tell_progress(message,progress);
end

tell_progress("Substituting Bases",progress)
xml.merge_bases()
progress = progress + 1;
tell_progress("Done Loading Data",1)
