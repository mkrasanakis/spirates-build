cprint = print
print = console.print
log = console.log
cdofile = dofile
dofile = loop.dofile
help  = console.help
clear = console.clear
_ = print
push = ai.push
pop  = ai.pop

shaders = assets.reload_shaders
xmls = assets.reload_xml
textures = assets.reload_textures
sounds = sound.clear_cache
reload = object.reload

function _pretty_print(a)
	print(serpent.block(a))
end

function tell_progress(m,p)
	log(m,4)
	loop.set_message(m)
	loop.set_progress(p)
	loop.refresh_frame()
end

function list(o)
	for key,value in pairs(o) do
		print('.' .. key);
	end
end

function directory_of(str)
    return str:match("(.*"..'/'..")")
end

function readonlytable(table)
   return setmetatable({}, {
     __index = table,
     __newindex = function(table, key, value)
                    error("Attempt to modify read-only table")
                  end,
     __metatable = false
   });
end

function lp()
	ai.load_player()
	ai.load_self()
end

function prn()
	lp()
	object.load_parent()
end

function script_path()
   local str = debug.getinfo(2, "S").source:sub(2)
   return str:match("(.*/)")
end


if object_attr_store == nil then object_attr_store = {} end
function set_temp_attribute(id,attr,val)
	if object_attr_store[id] == nil then object_attr_store[id] = {} end
	object_attr_store[id][attr] = val
	return object_attr_store[id][attr]
end
function get_temp_attribute(id,attr)
	if object_attr_store[id] then return object_attr_store[id][attr] end
	return nil
end

item.set_temp_attribute = function(attr,val) return set_temp_attribute(item.id(),attr,val) end
item.get_temp_attribute = function(attr) return get_temp_attribute(item.id(),attr) end

object.set_temp_attribute = function(attr,val) return set_temp_attribute(object.id(),attr,val) end
object.get_temp_attribute = function(attr) return get_temp_attribute(object.id(),attr) end

ai.set_temp_attribute = function(attr,val) return set_temp_attribute(ai.id(),attr,val) end
ai.get_temp_attribute = function(attr) return get_temp_attribute(ai.id(),attr) end


function test_item(name)
	lp()
	local limbs = {"right hand", "left hand", "body", "head"}
	local success = false
	local i
	for i=1,#limbs do
		if(ai.load_item(limbs[i])==false)then
			success = ai.create_limb_item(limbs[i], name)
			if(success)then
				break
			end
		end
	end
	object.load_parent()
	if(success==false)and(ship.load_object())then
		local n = ship.get_number_of_items()
		for i=0,n-1 do
			if(ship.load_item(i)==false)then
				success = ship.create_item(name, i)
				if(success)then
					break
				end
			end
		end
	end
	return success
end

function show_objects()
	lp()
	object.load_parent()
	local n = object.get_number_of_children()
	local i
	for i=0,n-1 do
		push()
		object.load_child(i)
		print(object.get_alias())
		pop()
	end
end


function make_weather(name, cloud)
	if(cloud==nil)then
		cloud = "cloud0"
	end
	lp()
	while object.type() ~= "planet" do
		r = object.get_r()
		t = object.get_theta()
		object.load_parent()
	end
	r = r+150
	planet.load_object()
	planet.new_cloud(r, t, cloud)
	planet.set_weather(name, t, 180)
	return r..'<'..t
end

function make_ship(name)
	lp()

	while object.type() ~= "planet" do
		r = object.get_r()
		t = object.get_theta()
		object.load_parent()
	end

	planet.load_object()
	planet.create_ship(r,t,name)
	return r..'<'..t
end

function make_ship_mob(name)
	lp()

	while object.type() ~= "planet" do
		r = object.get_r()
		t = object.get_theta()
		object.load_parent()
	end

	planet.load_object()
	planet.create_ship_mob(r,t,name)
	return r..'<'..t
end

function make_town(name)
	lp()
	
	while object.type() ~= "planet" do
		r = object.get_r()
		t = object.get_theta()
		object.load_parent()
	end
	
	planet.load_object()
	planet.create_town(r,t,1,name)
	return r..'<'..t
end

function make_npc(name, faction)
	if(faction==nil)then
		faction = ""
	end
	lp()
	local x = object.get_x()
	local y = object.get_y()
	object.load_parent()
	if(object.is_flat())then
		object.create_character(faction, x, y, name)
	else
		print('This macro works only for flat parent (i.e. inside a town or ship).')
	end
end

function make_building(name)
	lp()
	local x = object.get_x()
	local y = object.get_y()
	object.load_parent()
	if(object.is_flat())then
		object.create_building(x, y, name)
	else
		print('This macro works only for flat parent (i.e. inside a town or ship).')
	end
end

function POLICE()
	push()
	ai.load_player()
	ai.create_inventory_item("Police Hat (blue)")
	ai.create_inventory_item("Police Uniform")
	ai.create_inventory_item("Police Shield")
	ai.create_inventory_item("Incendiary Pistol (consumer)")
	pop()
	print("Added to inventory: Police Hat (blue), Police Uniform, Police Shield, Incendiary Pistol (consumer)")
end

function SPACE()
	push()
	ai.load_player()
	ai.create_inventory_item("Uranium 235")
	ai.create_inventory_item("Fusion Generator")
	ai.create_inventory_item("Hovership")
	ai.create_inventory_item("Enviromental Module")
	pop()
	print("Added to inventory: Uranium 235, Fusion Generator, Hovership, Enviromental Module")
end

function JEDI()
	push()
	ai.load_player()
	ai.create_inventory_item("Photon Sword (green)")
	ai.create_inventory_item("Headband (black)")
	pop()
	print("Added to inventory: Photon Sword (green), Headband (black)")
end


function NUCLEAR()
	push()
	ai.load_player()
	ai.create_inventory_item("Uranium 235")
	ai.create_inventory_item("Fusion Generator")
	pop()
	print("Added to inventory: Uranium 235, Fusion Generator")
end

function TESLA()
	push()
	ai.load_player()
	ai.create_inventory_item("Rocket Engine")
	ai.create_inventory_item("Scientist Coat")
	ai.create_inventory_item("EMP Riffle (space)")
	pop()
	print("Added to inventory: Rocket Engine, Scientist Coat, EMP Riffle (space)")
end

function CERN()
	push()
	ai.load_player()
	ai.create_inventory_item("Antimatter Generator")
	ai.create_inventory_item("Antihydrogen")
	pop()
	print("Added to inventory: Antimatter Generator, Antihydrogen")
end

function HUBBLE()
	push()
	ai.load_player()
	ai.create_inventory_item("Space Suit")
	ai.create_inventory_item("Space Plating")
	ai.create_inventory_item("Space Helm")
	ai.create_inventory_item("Antimatter Generator")
	ai.create_inventory_item("Antihydrogen")
	pop()
	print("Added to inventory: Space Equipment")
end

function gogame()
	loop.set_context(CONTEXT.GAME)
end
