require'string'
require'data/scripts/macros'
require'data/scripts/constants'

serpent = require("data/scripts/serpent")
lfs = require'lfs'

local save_dir = xml.get_value("save","file","name","path")
lfs.mkdir(save_dir);


xml.add_path_type("icon");
xml.add_path_type("texture");
xml.add_path_type("angleless_texture");
xml.add_path_type("back_texture");
xml.add_path_type("front_texture");
xml.add_path_type("side_texture");
xml.add_path_type("shield_texture");

xml.add_path_type("water_texture");
xml.add_path_type("atmosphere_texture");
xml.add_path_type("ground_texture");
xml.add_path_type("bg_ground_texture");
xml.add_path_type("fg_ground_texture");

xml.add_path_type("attribute|value");
xml.add_path_type("animation|frame|path");
xml.add_path_type("frame|path");
xml.add_path_type("music");
	
xml.add_path_type("path");
xml.add_path_type("script");
xml.add_path_type("ai");
xml.add_path_type("backstep");
xml.add_path_type("initialization");
xml.add_path_type("generation");
xml.add_path_type("refresh");
xml.add_path_type("portrait");

xml.add_path_type("sound");

xml.add_path_type("image");
xml.add_path_type("stage|start");
xml.add_path_type("stage|process");
xml.add_path_type("stage|complete");
xml.add_path_type("stage|objective|start");
xml.add_path_type("stage|objective|process");
xml.add_path_type("stage|objective|complete");
xml.add_path_type("stage|objective|fail");

xml.add_path_type("bg");
