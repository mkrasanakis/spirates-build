if(simulation.speed()<2)then
	local x = 1/simulation.speed()
	if(x<3)then
		x = x+1
	end
	x = 1.0/math.floor(x+0.5)
	simulation.speed(x)
else
	simulation.speed(math.floor(simulation.speed()-1+0.5))
end
if(simulation.speed()>=1)then
	loop.message("Speed x"..math.floor(simulation.speed()+0.5))
else
	loop.message("Speed /"..math.floor(1.0/simulation.speed()+0.5))
end
