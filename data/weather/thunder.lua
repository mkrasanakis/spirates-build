local t = {}

t.on_spawn = function()
	local prev_size_y = item.get_size_y()
	object.set_size_y(40+object.get_r()-planet.get_water_r(object.get_theta()/360*planet.get_table_sizes()))
	object.set_size_x(item.get_size_x()*object.get_size_y()/prev_size_y)
	object.set_r(object.get_r()-object.get_size_y()/2+20)
	item.set_size_y(object.get_size_y())
	item.set_size_x(object.get_size_x())
	object.make_sound('data/weather/thunder.ogg', 0, 600, 1)
	object.apply_velocity(0, 0)
	object.set_mass(0)
end

t.on_process = function()
	item.set_state(item.get_state()+1)
	item.set_light_radius(400+200*math.sin(3.14159*item.get_state()/2))
end

return t