local t={}

--on_process
t.on_process = function()
	if(math.random()<0.2*math.max(0.5, math.min(object.get_prosperity(), 5))/60.0)then--execute <prosperity> times per minute with maximum 5 times per minute
		if(math.random()<0.5)then
			building.create_item("Gold", 0)
		end
		if(math.random()<0.5)then
			building.create_item("Gold", 1)
		end		
	end
end

return t