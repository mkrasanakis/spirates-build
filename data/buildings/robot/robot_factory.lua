local t={}

--on_process
t.on_process = function()
	local total_power = 0
	local i
	for i=0,building.get_number_of_items()-1 do 
		if(building.load_item(i))then
			total_power = total_power + item.get_generation()
		end
	end
	building.set_spawn_speed_mult(total_power/25.0)--so as for 3x diesel generator = 3x8 = 24 engine power to have normal spawn rate of 1
end

return t