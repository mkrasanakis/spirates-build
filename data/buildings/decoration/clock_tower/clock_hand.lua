local t = {}

t.on_process = function()
	local local_time = object.get_time()
	
	local angle
	if(item.get_grade()==0)then
		angle = 360*(1-local_time)*12
	elseif(item.get_grade()==1)then
		angle = math.floor(12*(1-local_time))/12*360
	end
	angle = -angle
	item.set_angle(angle)
	item.set_position_x(-item.get_size_y()*math.sin(angle/360*2*3.14159)/2)
	item.set_position_y(item.get_size_y()*math.cos(angle/360*2*3.14159)/2)
end

return t