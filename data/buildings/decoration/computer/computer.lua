local t={}

--on_process
t.on_update = function()
	if(building.load_item(0))then
		item.clear_attributes()
		
		local names = {}
		local mult = {}
		local global_mult = 1
		local i
		local j
		-- get multipliers
		for i = 1,8 do 
			push()
			if(building.load_item(i))then
				local repo_name = item.get_name()
				local found = false
				for j = 0,#names-1 do
					if(repo_name==names[j])then
						found = true
						break
					end
				end
				if(found==false)then
					names[#names] = repo_name
					local repo_type = item.get_attribute("type")
					if(repo_type==0)then
						if(item.get_attribute("multiplies")~=0)then
							global_mult = global_mult*item.get_attribute("multiplies")
						end
					else
						if(mult[repo_type]==nil)then
							mult[repo_type] = 1
						end
						if(item.get_attribute("multiplies")~=0)then
							mult[repo_type] = mult[repo_type]*item.get_attribute("multiplies")
						end
					end
				end
			end
			pop()
		end
		-- get actual values
		names = {}
		local extra_chip_size = 0
		local desc = ""
		for i = 1,8 do 
			push()
			if(building.load_item(i))then
				local repo_name = item.get_name()
				local found = false
				for j = 0,#names-1 do
					if(repo_name==names[j])then
						found = true
						break
					end
				end
				if(found==false)then
					names[#names] = repo_name
					local repo_type = item.get_attribute("type")
					local stat = item.get_attribute("stat")
					local value = item.get_attribute("value")
					local global_error = item.get_attribute("fault")
					local size = item.get_grade()+1
					if(mult[repo_type]==nil)then
						mult[repo_type] = 1
					end
					desc = desc.."\n+ "..repo_name.." ("..size.." kB)"
					extra_chip_size = extra_chip_size+item.get_attribute("ChipSize")*mult[repo_type]*global_mult
					pop()
						item.set_attribute("error", item.get_attribute("error")+global_error)
						item.set_attribute("size", item.get_attribute("size")+size)
						if(repo_type~=0)and(stat~=0)then
							--desc = desc.." x"..mult[repo_type]*global_mult
							item.set_attribute(stat, item.get_attribute(stat)+value*mult[repo_type]*global_mult)
						end
					push()
					--building.load_item(i)
				end
			end
			pop()
		end
		if(desc=="")then
			item.set_description("Empty chip: a computer and repos are needed to program chips")
		else
			item.set_description("Repos:"..desc)
		end
		
		local attribute_list = {"Melee Combat",
								"Ranged Combat",
								"Endurance",
								"Max Health",
								"Speed",
								"Navigation"}
		desc = ""
		for i=1,#attribute_list do
			if(item.get_attribute(attribute_list[i])~=0)then
				desc = desc.."+"..math.ceil(item.get_attribute(attribute_list[i])).." "..attribute_list[i].."\n"
			end
		end
		item.set_short_description(desc)
		
		local max_size = math.pow(2,item.get_grade()+1)*(1+extra_chip_size*0.5)
		if(item.get_attribute("size")>max_size)then
			item.set_attribute("error", math.min(item.get_attribute("error")+(item.get_attribute("size")-max_size*1.0)/max_size, 1))
		end
		local fault = ""
		if(item.get_attribute("error")>0)then
			fault = " (unstable "..math.ceil(item.get_attribute("error")*100).."%)"
		end
		if(extra_chip_size>0)then
			fault = fault.."+"..(max_size-math.pow(2,item.get_grade()+1)).."kB"
		elseif(extra_chip_size<0)then
			fault = fault..(max_size-math.pow(2,item.get_grade()+1)).."kB"
		end
		item.set_alias("Chip".."  "..item.get_attribute("size").."kB/ "..(math.pow(2,item.get_grade()+1)).."kB"..fault)
		item.set_dirty_save(1)
		loop.message(item.get_alias())
	end
	--return true
end

return t