local dx = 0.6 + math.random()*0.8
local dy = dx - 0.2 + math.random()*0.4

object.set_size_x(object.get_size_x()*dx)
object.set_size_y(object.get_size_y()*dy)