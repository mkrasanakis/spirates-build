local t={}

t.get_random_item = function()
	building.load_self()
	xml.load_node(object.get_name(), "building", "name")
	local generate_name = xml.get_value("produce")
	if(generate_name==nil)then
		return ""
	end
	local possible_item_list = xml.list(generate_name, "tag", "item")
	local possible_item_prop = {}
	local inc
	possible_item_prop[1] = 0
	local selected_item = ""
	xml.push()
	for inc=1,#possible_item_list do 
		xml.load_node(possible_item_list[inc], "item", "name")
		local temp
		temp = xml.get_value("grade")
		if(temp~="-1")then
			temp = xml.get_value("probability")
			if(temp==nil)then
				possible_item_prop[inc+1] = possible_item_prop[inc]+1
			else
				possible_item_prop[inc+1] = possible_item_prop[inc]+tonumber(temp)
			end
		else
			possible_item_prop[inc+1] = possible_item_prop[inc]--ignore this way
		end
	end
	xml.pop()
	--select an item from the list
	local prop = math.random()*possible_item_prop[#possible_item_prop]
	for inc=1,#possible_item_list do 
		if(prop<possible_item_prop[inc+1])then
			selected_item = possible_item_list[inc]
			break
		end
	end
	return selected_item
end

--on_process
t.on_update = function()
	if(building.load_item(0))then
		loop.message("Take the previously won "..item.get_alias().." to play again")
	elseif(ai.load_player())and(ai.get_credits()>500)then
		ai.add_credits(-500)
		sound.play_sound("data/buildings/casino/slot_machine/ding.ogg", 1)
		if(rng.random(0,1,1)<0.5)then
			local selected_item = t.get_random_item()
			if(selected_item~="")then
				building.create_item(selected_item, 0)
				loop.message("Placed a 500$ bet ... and won "..item.get_alias())
			end
		else
			local selected_item = "Duck Toy"
			building.create_item(selected_item, 0)
			loop.message("Placed a 500$ bet ... and won "..item.get_alias())
		end
	else
		loop.message("500$ are meeded to play")
	end
end

return t