require("data/characters/human/combat_ai/perform_ai_mood")
require("data/characters/human/combat_ai/perform_ai_conversation")
require("data/characters/human/combat_ai/perform_ai_get_target")
require("data/characters/human/combat_ai/perform_ai_get_ship_target")
require("data/characters/human/combat_ai/perform_ai_equip")

--find target
local target = perform_ai_get_target()

--handle mood and conversation
if(ai.get_health()<ai.get_max_health()*0.9)or(target~=-1)or(ai.get_stun()>0)then
	ai.end_speech()
elseif(ai.can_speak())then
	perform_ai_conversation()
end
perform_ai_mood()

--load parent size
ai.load_self()
object.load_parent()
local parent_size_x = object.get_size_x()

--perform either combat targeting or patrol
if(target~=-1)then
	--combat targeting depends solely on what makes weapon usage efficient
	local distance = ai.get_distance(target)
	local move_closer = 0
	if(ai.load_item("right hand"))then
		local melee_damage = item.get_melee_damage()
		local ranged_damage = item.get_ranged_damage()
		local in_shooting_range = (distance<item.get_shooting_range()*10)and(ranged_damage>0)
		local in_melee_range = (distance<item.get_range())and(melee_damage>0)
		if(in_shooting_range or in_melee_range or ((item.get_defense()>0)and(ai.get_air()>2)) )then
			ai.swing_right()
		end
		if(in_shooting_range)then
			move_closer = move_closer - ranged_damage
		end
		if(melee_damage>0)and(in_melee_range==false)then
			move_closer = move_closer + melee_damage
		end
	end
	if(ai.load_item("left hand"))then
		local melee_damage = item.get_melee_damage()
		local ranged_damage = item.get_ranged_damage()
		local in_shooting_range = (distance<item.get_shooting_range()*10)and(ranged_damage>0)
		local in_melee_range = (distance<item.get_range())and(melee_damage>0)
		if(in_shooting_range or in_melee_range or ((item.get_defense()>0)and(ai.get_air()>2)) )then
			ai.swing_left()
		end
		if(in_shooting_range)then
			move_closer = move_closer - ranged_damage
		end
		if(melee_damage>0)and(in_melee_range==false)then
			move_closer = move_closer + melee_damage
		end
	end
	ai.move_towards(target)
	if(move_closer>0)then
		ai.unswing_right()
		ai.unswing_left()
	else
		ai.move_stop()
		if(ai.get_attribute("crouch")>0)then
			ai.set_attribute("crouch", ai.get_attribute("crouch")-1)
			ai.move_down()
		elseif(math.random()<0.2)then
			ai.set_attribute("crouch", math.random(5,10))
			ai.move_down()
		end
	end
else
	local patrol = false
	if(ai.is_captain())then
		local enemy_target = get_ship_target()
		if(enemy_target~=-1)then
			--move towards enemy ship or town, attack if simply nearby and board if possible
			ai.load_ship()
			local own_shields = ship.get_shield()
			ai.load_self()
			object.load_parent()
			local r = object.get_r()
			local t = object.get_theta()
			local size_r = object.get_size_y()
			local size_t = object.get_size_x()/2/r*180/3.14159
			local dir = object.get_direction()
			object.load_by_id(enemy_target)
			local target_r = object.get_r()
			local target_t = object.get_theta()
			local target_shield = 0
			if(ship.load_object())then
				target_shield = ship.get_shield()
			end
			size_t = size_t + object.get_size_x()/2/target_r*180/3.14159
			--if controlling ship move towards target, otherwise just patrol. if within target range, try to move on
			if(t<target_t-size_t*2)then
				if(ai.in_ship())then
					ai.move_left()
				else
					patrol = true
				end
			elseif(t>target_t+size_t*2)then
				if(ai.in_ship())then
					ai.move_right()
				else
					patrol = true
				end
			else
				--change ship to face towards enemy
				if(ai.in_ship())then
					if(math.abs(t - target_t)<size_t*0.2)then
						if(t>target_t)then
							ai.move_left()
						else
							ai.move_right()
						end
					elseif(dir*(t - target_t)>0)and(math.abs(t - target_t)>size_t*0.5)then
						if(t>target_t)then
							ai.move_right()
						else
							ai.move_left()
						end
					elseif(math.abs(t - target_t)>size_t*0.4)then
						ai.move_stop()
					end
				end
				--try to board enemy ship
				if(math.random()<0.5)and(target_shield==0)and(own_shields==0)then
					ai.change_parent()
				end
			end
			if(math.abs(t - target_t)<2*size_t)and(dir*(t - target_t)<0)and(object.is_ship())and(ai.in_ship())then--target within range, target is ship and ai is captain
				ai.swing_left()
				ai.swing_right()
			end
		else
			--patrol if nothing else to do
			patrol = true
		end
	elseif(ai.in_ship())and(ai.get_direct_superior()==-1)then
		--try to control ship if no enemies and highest rank
		ai.load_self()
		object.load_parent()
		ship.load_object()
		local control_x = ship.get_control_x()
		local control_y = ship.get_control_y()
		ai.load_self()
		if(object.get_x()<control_x-object.get_size_x())then
			ai.move_right()
		elseif(object.get_x()>control_x+object.get_size_x())then
			ai.move_left()
		else
			ai.move_stop()
			ai.control_parent()
		end
	else
		patrol = true
	end
	if(patrol)then
		--possible patrol is performed at frequent intervals
		local patrol_counter = ai.get_attribute("patrol counter")-1
		if(patrol_counter<0)then
			local r = math.random()
			if(r<0.2)then
				ai.move_left()
			elseif(r<0.4)then
				ai.move_right()
			else
				ai.move_stop()
			end
			patrol_counter = 5+math.random(10)
		end
		ai.set_attribute("patrol counter", patrol_counter)
	end
	if(math.random()<0.1)then
		perform_ai_equip(target~=-1, false)
	end
end

--stop movement if stunned, speaking or about to fall of
ai.load_self()
local direction = -object.get_direction()
if(ai.get_stun()>0)or(ai.is_speaking())or((0.5*parent_size_x*direction-object.get_x())*direction<0)then
	ai.move_stop()
end
--vanish if fell on water
object.load_parent()
if(object.is_planet())then
	ai.vanish()
end