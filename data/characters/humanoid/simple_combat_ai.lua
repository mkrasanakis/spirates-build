require("data/characters/humanoid/combat_ai/perform_ai_mood")
require("data/characters/humanoid/combat_ai/perform_ai_conversation")
require("data/characters/humanoid/combat_ai/perform_ai_get_target")
require("data/characters/humanoid/combat_ai/perform_ai_get_ship_target")
require("data/characters/humanoid/combat_ai/perform_ai_equip")

--find target
local target = -1
local urgent_patrol = false
local in_planet = false
--load parent size
ai.load_self()
object.load_parent()
local parent_size_x = object.get_size_x()

if(ai.is_captain()==false)then
--if fell on water, patrol will work normally, so just make sure to never stop moving
ai.load_self()
object.load_parent()
if(object.is_planet())then
	ai.set_attribute("to_tell_searching", 0)
	patrol = true
	urgent_patrol = true
	ai.move_up()
	in_planet = true
else
	target = perform_ai_get_target()
end

--handle mood and conversation
--[[
if(ai.get_health()<ai.get_max_health()*0.9)or(target~=-1)or(ai.get_stun()>0)then
	ai.end_speech()
elseif(ai.can_speak())then
	perform_ai_conversation()
end
perform_ai_mood()
--]]

if(ai.can_speak())then
	perform_ai_conversation()
end

--if target in invalid position, patrol to move randomly
if(target~=-1)and(urgent_patrol==false)then
	push()
	ai.load_self()
	local ai_y = object.get_y()
	object.load_by_id(target)
	if(ai_y>object.get_y()+object.get_size_y())or(ai_y<object.get_y()-object.get_size_y())then
		target = -1
		urgent_patrol = true
	end
	pop()
end

end--end check for is captain

--perform either combat targeting or patrol
if(target~=-1)then
	if(ai.in_combat_mode()==false)then
		ai.trigger_combat()
		push()
		object.load_by_id(target)
		local text
		if(object.get_alias()==object.get_name())then
			text = "a member of "..object.get_faction()
		else
			text = "the famous criminal "..object.get_alias()
		end
		ai.load_self()
		if(ai.trigger_nearby_combat()>0)then
			ai.global_speak("alarm", "Look! There is "..text)
		else
			ai.global_speak("alarm", "You! You are "..text.."!")
		end
		pop()
	end

	ai.set_attribute("patrol counter", 0)
	--calculate character distance from target
	local distance = ai.get_distance(target)
	--combat targeting depends solely on what makes weapon usage efficient
	local move_closer = 0
	local move_crouch = 0
	local shooting_range = 500
	local total_item_damage = 0
	local health_frac = ai.get_health()/ai.get_max_health()
	if(ai.load_item("right hand"))then
		local melee_damage = item.get_melee_damage()
		local ranged_damage = item.get_ranged_damage()
		local in_shooting_range = (distance<item.get_shooting_range()*5)and(ranged_damage>0)
		local in_melee_range = (distance<item.get_range()/2)and(melee_damage>0)
		total_item_damage = total_item_damage + melee_damage + ranged_damage
		if(item.get_shooting_range()>0)and(item.get_shooting_range()<shooting_range)then
			shooting_range = item.get_shooting_range()
		end
		if((in_shooting_range and(ai.get_air()>(item.get_grade()+2)*math.random())) or in_melee_range or ((item.get_defense()>0)and(ai.get_air()>2+distance*(1.5-health_frac)/2)) )then
			ai.swing_right()
		end
		if(in_shooting_range)then
			move_closer = move_closer - ranged_damage
			move_crouch = move_crouch + ranged_damage
		end
		if(melee_damage>0)and(in_melee_range==false)then
			move_closer = move_closer + melee_damage
		end
		if(in_melee_range)then
			move_crouch = move_crouch - melee_damage
		end
	end
	if(ai.load_item("left hand"))then
		local melee_damage = item.get_melee_damage()
		local ranged_damage = item.get_ranged_damage()
		local in_shooting_range = (distance<item.get_shooting_range()*5)and(ranged_damage>0)
		local in_melee_range = (distance<item.get_range()/2)and(melee_damage>0)
		total_item_damage = total_item_damage + melee_damage + ranged_damage
		if(item.get_shooting_range()>0)and(item.get_shooting_range()<shooting_range)then
			shooting_range = item.get_shooting_range()
		end
		if((in_shooting_range and(ai.get_air()>(item.get_grade()+2)*math.random())) or in_melee_range or ((item.get_defense()>0)and(ai.get_air()>2+distance*(1.5-health_frac)/2)) )then
			ai.swing_left()
		end
		if(in_shooting_range)then
			move_closer = move_closer - ranged_damage
			move_crouch = move_crouch + ranged_damage
		end
		if(melee_damage>0)and(in_melee_range==false)then
			move_closer = move_closer + melee_damage
		end
		if(in_melee_range)then
			move_crouch = move_crouch - melee_damage
		end
	end
	if(math.random()<0.1*(0.8-ai.get_health()/ai.get_max_health()))then
		if(math.random()<0.5)then
			ai.use_alt_left()
		else
			ai.use_alt_right()
		end
	end
	if(move_closer<0)then
		move_closer = -shooting_range/4
	end
	if(total_item_damage<=0)then
		ai.move_away(target, 1)
		ai.set_attribute("fleeing", 1)
	elseif(move_closer>=0)then
		ai.move_towards(target)
		ai.set_attribute("fleeing", 0)
	elseif(math.abs(move_closer)>math.abs(distance))and(ai.running_animation("hand rise")==false)then
		--try to flee (by sprinting if enough air)
		if(math.random()<0.3*(1.5-0.1*ai.get_rank()))or(ai.get_attribute("fleeing")==1)then
			ai.set_attribute("crouch", 0)
			if(ai.get_air()>ai.get_max_air()/3)then
				ai.move_away(target, 2)
			else
				ai.move_away(target, 1)
			end
			move_closer = -move_closer
			ai.set_attribute("fleeing", 1)
		else
			move_closer = -move_closer
		end
	else
		ai.move_towards(target)
		ai.set_attribute("fleeing", 0)
	end
	ai.load_self()
	if(move_closer<=0)and(total_item_damage>0)then--or(move_closer<0 and math.abs(move_closer)+object.get_size_x()/2<math.abs(distance)))then
		ai.move_stop()
		ai.set_attribute("fleeing", 0)
		--[[
		if(ai.get_attribute("crouch")>0)then
			ai.set_attribute("crouch", ai.get_attribute("crouch")-1)
			ai.move_towards(target)
			if(ai.facing(target))then
				ai.move_down()
				ai.set_attribute("crouch", ai.get_attribute("crouch")-1)
			else
				ai.set_attribute("crouch", 0)
			end
		elseif(math.random()<0.01*move_crouch)and(ai.facing(target))then
			ai.set_attribute("crouch", math.random(10,20))
			ai.move_towards(target)
			ai.move_down()
		end
		--]]
	end
	
	if(ai.get_attribute("to_tell_searching")==2)and(ai.get_stun()==0)then
		if(ai.rally_against(target)>0)then
			ai.global_speak("rally")
		end
	elseif(math.random()<0.3)and((total_item_damage>0)or(ai.get_health()<ai.get_max_health()))and(ai.get_stun()==0)then
		if(ai.trigger_nearby_combat()>0)then
			push()
			ai.load_self()
			if(total_item_damage>0)then
				ai.global_speak("alarm")
			else
				ai.global_speak("help")
			end
			pop()
		end
	elseif(math.random()<0.02)and(total_item_damage>0)and(ai.get_stun()==0)then
		ai.global_speak("die")
	end
	if(total_item_damage>0)then
		ai.set_attribute("to_tell_searching", 1)
	else
		ai.set_attribute("to_tell_searching", -1)
	end
else
	if(ai.in_combat_mode())and(ai.get_attribute("to_tell_searching")==1)and(math.random()<0.2)and(ai.get_stun()==0)then
		if(math.random()<0.3)then
			ai.set_attribute("to_tell_searching", 2)
			push()
			ai.load_self()
			ai.global_speak("searching")
			pop()
		else
			ai.set_attribute("to_tell_searching", 0)
		end
	end
	local patrol = false
	if(ai.is_captain())then
		local enemy_target = get_ship_target()
		if(enemy_target~=-1)then
			--move towards enemy ship or town, attack if simply nearby and board if possible
			ai.load_ship()
			local own_shields = ship.get_shield()
			ai.load_self()
			object.load_parent()
			local r = object.get_r()
			local t = object.get_theta()
			local size_r = object.get_size_y()
			local size_t = object.get_size_x()/2/r*180/3.14159
			local dir = object.get_direction()
			object.load_by_id(enemy_target)
			local target_r = object.get_r()
			local target_t = object.get_theta()
			local target_shield = 0
			if(ship.load_object())then
				target_shield = ship.get_shield()
			end
			size_t = size_t + object.get_size_x()/2/target_r*180/3.14159
			--if controlling ship move towards target, otherwise just patrol. if within target range, try to move on
			if(t<target_t-size_t*2)then
				if(ai.in_ship())then
					ai.move_left()
				else
					patrol = true
				end
			elseif(t>target_t+size_t*2)then
				if(ai.in_ship())then
					ai.move_right()
				else
					patrol = true
				end
			else
				--change ship to face towards enemy
				if(ai.in_ship())then
					if(math.abs(t - target_t)<size_t*0.2)then
						if(t>target_t)then
							ai.move_left()
						else
							ai.move_right()
						end
					elseif(dir*(t - target_t)>0)and(math.abs(t - target_t)>size_t*0.5)then
						if(t>target_t)then
							ai.move_right()
						else
							ai.move_left()
						end
					elseif(math.abs(t - target_t)>size_t*0.4)then
						ai.move_stop()
					end
				end
				--try to board enemy ship
				if(math.random()<0.5)and(target_shield==0)and(own_shields==0)then
					ai.change_parent()
				end
			end
			if(math.abs(t - target_t)<2*size_t)and(dir*(t - target_t)<0)and(object.is_ship())and(ai.in_ship())then--target within range, target is ship and ai is captain
				ai.swing_left()
				ai.swing_right()
			end
		else
			--patrol if nothing else to do
			patrol = true
		end
	elseif(ai.in_ship())and(ai.is_ship_owner())then--and(ai.get_direct_superior()==-1)then
		--//try to control ship if no enemies and highest rank
		--try to control ship if no enemies and owns a ship (only original captains from ship creation own a ship as npcs)
		ai.load_self()
		object.load_parent()
		ship.load_object()
		local control_x = ship.get_control_x()
		local control_y = ship.get_control_y()
		ai.load_self()
		if(object.get_x()<control_x-object.get_size_x())then
			ai.move_right()
		elseif(object.get_x()>control_x+object.get_size_x())then
			ai.move_left()
		else
			ai.move_stop()
			ai.control_parent()
		end
	else
		patrol = true
	end
	if(patrol)then
		ai.load_self()
		--possible patrol is performed at frequent intervals
		local patrol_counter = ai.get_attribute("patrol counter")-1
		if(patrol_counter<0)then
			local r = math.random()
			if(urgent_patrol)then
				r = r*0.4
			end
			if(object.get_x()>parent_size_x*0.4)then
				ai.move_left()
			elseif(object.get_x()<-parent_size_x*0.4)then
				ai.move_right()
			elseif(r<0.2)then
				ai.move_left()
			elseif(r<0.4)then
				ai.move_right()
			elseif(r<0.7)then
				ai.move_stop()
			end
			patrol_counter = 20+math.random(20)
			if(urgent_patrol)then
				patrol_counter = patrol_counter*5
			end
		end
		if(ai.on_ladder())then
			if(ai.get_attribute("on ladder")==0)then
				if(math.random()<0.3)or(urgent_patrol)then
					ai.set_attribute("on ladder", 1)
				else
					ai.set_attribute("on ladder", -1)
				end
			end
			if(ai.get_attribute("on ladder")==1)then
				ai.move_up()
			end
		else
			ai.set_attribute("on ladder", 0)
		end
		ai.set_attribute("patrol counter", patrol_counter)
	end
	if(math.random()<0.02*(10-ai.get_rank()))and(in_planet==false)then
		perform_ai_equip(target~=-1, false)
	end
end


--eat from alt slots if too low health
if(ai.get_health()<0.8*ai.get_max_health())and(math.random()<0.05)then
	if(math.random()<0.5)then
		ai.use_alt_left()
	else
		ai.use_alt_right()
	end
end

--stop movement if stunned, speaking or about to fall of
ai.load_self()
local direction = -object.get_direction()
if(ai.get_stun()>0)or((target==-1)and(in_planet==false)and((ai.is_speaking())or(((parent_size_x/2-ai.get_speed())*direction-object.get_x())*direction<0)))then
	ai.move_stop()
end
--small chance to move to parent
if(in_planet)and(math.random()<0.02)then
	ai.change_parent()
end