local t = {}

t.on_complete = function(group)
	-- loads the left hand limb and then attacks with the item inside
	if(ai.load_item(ai.get_limb("hand", group)))then
		ai.attack_with_item()
	end
end

return t