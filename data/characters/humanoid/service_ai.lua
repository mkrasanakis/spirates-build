require("data/characters/humanoid/combat_ai/perform_ai_mood")
require("data/characters/humanoid/combat_ai/perform_ai_service_conversation")
require("data/characters/humanoid/combat_ai/perform_ai_vanish")

perform_ai_mood()
if(ai.can_speak())then
	perform_ai_service_conversation()
end

--if enough time outside vanish
if(ai.get_health()>0)and(ai.is_speaking()==false)then
	local inc=1
	if(ai.get_health()<ai.get_max_health())then
		inc = 10
	end
	perform_ai_vanish(inc)
end

--if speaking do not move
if(ai.is_speaking())then
	ai.move_stop()
end