require("data/characters/humanoid/combat_ai/perform_ai_mood")
require("data/characters/humanoid/combat_ai/perform_ai_conversation")
require("data/characters/humanoid/combat_ai/perform_ai_vanish")

perform_ai_mood()
if(ai.can_speak())then
	perform_ai_conversation()
end

if(ai.is_speaking())then
	ai.move_stop()
elseif(ai.get_health()<ai.get_max_health())then
	perform_ai_vanish(5)
	ai.move_stop()
else
	perform_ai_vanish(1)
	local patrol_counter = ai.get_attribute("patrol counter")-1
	if(patrol_counter<0)then
		local r = math.random()
		if(r<0.2)then
			ai.move_left()
		elseif(r<0.4)then
			ai.move_right()
		else
			ai.move_stop()
		end
		patrol_counter = 5+math.random(10)
	end
	ai.set_attribute("patrol counter", patrol_counter)
end
