require("data/dialog/conversation_ai")

function perform_ai_conversation()
	if(ai.is_speaking())then
		perform_conversation()
	elseif(ai.can_speak())then
		local speech_timer = ai.get_attribute("speech_timer")+1
		if(speech_timer>300)then
			speech_timer = 0
			ai.set_attribute("speech_timer", 0)
			perform_conversation()
		else
			ai.set_attribute("speech_timer", speech_timer)
		end
	end
end