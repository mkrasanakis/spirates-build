function perform_ai_get_target()
	local target_counter = ai.get_attribute("retarget")-1
	local target = -1
	if(target_counter<=0)then
		local search_dist = 300 --100*math.random()^(3-simulation.difficulty()/2)+(10-ai.get_rank())*7
		local relation_threshold = 0--math.min(-1-ai.get_attribute("mood")/300, 0)
		local bounty_unit = 5000
		if(ai.in_ship())then
			bounty_unit = 1000
			if(ai.is_ship_owner())then
				relation_threshold = relation_threshold+3
			end
		end
		if(ai.in_combat_mode())then
			target = ai.get_target(search_dist, relation_threshold, 1.0/(1+ai.get_speed()), 1.0/(1+ai.get_health()), 1.0/(1+ai.get_rank())/bounty_unit, 100)
		elseif(math.random()<0.01)then
			bounty_unit = 1000*(5-simulation.difficulty())
			search_dist = search_dist/2
			target = ai.get_target(search_dist, -10, 0, 0, 1.0/(1+ai.get_rank())/bounty_unit, -1)
		end
		target_counter = ai.get_rank()/2
		ai.set_attribute("target", target+1)
	else
		target = ai.get_attribute("target")-1
		if(target~=-1)and(ai.in_same_parent(target)==false)then
			target = -1
			ai.set_attribute("target", 0)
		end
	end
	ai.set_attribute("retarget", target_counter)
	return target
end