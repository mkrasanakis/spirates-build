require("data/characters/humanoid/combat_ai/equip_slot")

function perform_ai_equip(has_target, in_melee_range)
	local equip_delay = ai.get_attribute("performed_equip")
	if(equip_delay<=0)then
		equip_delay = 100
		local attack_frac = 0.8*ai.get_health()/ai.get_max_health()
		if(has_target==false)then
			equip_slot("right hand", 1, 1, 1)
			equip_slot("left hand" , 1, 1, 1)
			equip_slot("body", attack_frac, attack_frac, 1)
			equip_slot("head", attack_frac, attack_frac, 1)
		elseif(in_melee_range)then
			equip_slot("right hand", 2, 1, 1)
			equip_slot("left hand" , 2, 1, 1)
			equip_slot("body", attack_frac, attack_frac, 1)
			equip_slot("head", attack_frac, attack_frac, 1)
		else
			equip_slot("right hand", 0, 1, 1)
			equip_slot("left hand" , 0, 1, 1)
			equip_slot("body", attack_frac, attack_frac, 1)
			equip_slot("head", attack_frac, attack_frac, 1)
		end
	end
	ai.set_attribute("performed_equip", equip_delay-10)
	return
end