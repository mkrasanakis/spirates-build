function get_ship_target()
	local target_counter = ai.get_attribute("retarget_ship")-1
	local target = -1
	if(target_counter<=0)then
		local faction = ai.get_faction()
		local min_value = 0
		ai.load_self()
		object.load_parent() --loaded ship or town
		if(object.is_flat())then
			local t = object.get_theta()
			local r = object.get_r()
			object.load_parent() --loaded planet
			if(object.is_planet())then
				local i
				local n
				local obj_faction
				local dist
				local value
				n = object.get_number_of_children()
				for i=0,n-1 do
					object.load_child(i)
					if(object.is_ship())then
						obj_faction = object.get_faction()
						dist = r*math.abs(object.get_theta()-t)/360*2*3.14159--calculate distance
						value = ai.get_relation(obj_faction)-ai.get_bounty(obj_faction)/1000+dist/300--lower values are more desirable targets
						if(value<min_value)then
							min_value = value
							target = object.id()
						end
					end
					object.load_parent()
				end
			end
		end
		target_counter = 10+math.random(10)
		ai.set_attribute("target_ship", target+1)
	else
		target = ai.get_attribute("target_ship")-1
	end
	ai.set_attribute("retarget_ship", target_counter)
	return target
end