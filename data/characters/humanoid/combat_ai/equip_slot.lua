----------------------------------------------------------------------------------------------------------------------------------
--Function   : equip_slot
--Description: equips best item from inventory to designated slot
----------------------------------------------------------------------------------------------------------------------------------
function equip_slot(name, melee_value, ranged_value, defense_value)
	local best_rating
	local best_id
	local rating
	--item in slot initializes best rating
	best_rating = -1
	best_id     = -1
	if(ai.load_item(name))then
		rating = item.get_melee_damage()*melee_value+item.get_ranged_damage()*ranged_value+item.get_defense()*defense_value
		if(string.find(item.get_name(), "Headband") or string.find(item.get_name(), "Martial Belt") )then
			rating = rating + (item.get_grade()+2)*math.max(melee_value,ranged_value)
		end
		best_rating = rating
	end
	--get best rating from inventory
	local i
	for i = 0, ai.get_inventory_size()-1 do
		if(ai.load_inventory(i))and(ai.can_equip_item(name, i))then
			rating = item.get_melee_damage()*melee_value+item.get_ranged_damage()*ranged_value+item.get_defense()*defense_value
			if(string.find(item.get_name(), "Headband") or string.find(item.get_name(), "Martial Belt") )then
				rating = rating + (item.get_grade()+2)*math.max(melee_value,ranged_value)
			end
			if(rating>best_rating)then
				best_id     = i
				best_rating = rating
			end
		end
	end
	--if found something in inventory with better rating equip that
	if(best_id~=-1)then
		ai.equip_item(name, best_id)
	end
	return
end