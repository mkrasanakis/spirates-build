function add_bounty(target)
	local target_faction
	local target_reward
	local target_rank
	push()
		ai.load_self()
		object.load_parent()
		object.load_child(target)
		ai.load_object()
		target_faction = ai.get_faction()
		target_reward  = ai.get_reward()
		target_rank    = math.max(0, ai.get_rank())
	pop()
	if(ai.get_relation(target_faction)<0)then
		ai.add_bounty(target_faction, -ai.get_relation(target_faction)*math.log(ai.get_credits()+100)/(1+target_rank))
	end
	return
end