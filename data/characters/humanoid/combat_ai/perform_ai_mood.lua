function perform_ai_mood()
	local ai_mood = ai.get_attribute("mood")
	local mood_diff = 0.5
	if(ai_mood>=mood_diff)then
		ai.set_attribute("mood", ai_mood-mood_diff)
	elseif(ai_mood<=-mood_diff)then
		ai.set_attribute("mood", ai_mood+mood_diff)
	else
		ai.set_attribute("mood", 0)
	end
	return
end