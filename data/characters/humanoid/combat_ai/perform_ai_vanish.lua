function perform_ai_vanish(speed)
	local vanish = ai.get_attribute("vanish")
	if(vanish>100)then
		ai.vanish()
	else
		ai.set_attribute("vanish", vanish+speed)
	end
	return
end