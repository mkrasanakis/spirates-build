local t = {}

t.on_complete = function(group)
	if(ai.is_player()==false)then
		if(group=="right")then
			ai.unswing_right()
		elseif(group=="left")then
			ai.unswing_left()
		end
	end
end

return t