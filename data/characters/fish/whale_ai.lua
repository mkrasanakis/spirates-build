local patrol_counter = ai.get_attribute("patrol counter")-1
if(patrol_counter<0)then
	local r = math.random()
	if(r<0.5)then
		ai.move_left()
	else
		ai.move_right()
	end
	patrol_counter = 50+math.random(100)
end
ai.set_attribute("patrol counter", patrol_counter)
if(math.random()<0.5)then
	ai.move_up()
elseif(ai.get_air()<ai.get_max_air()/10)then
	ai.move_up()
elseif(ai.get_air()>=ai.get_max_air())then
	ai.move_down()
end