function get_graded_item(name, min_grade)
	if(min_grade<0)then
		return name
	end
	local r = math.random()
	if(min_grade>=1)and(r<0.5)then
		r = 0.5
	end
	if(min_grade>=2)and(r<0.75)then
		r = 0.75
	end
	if(min_grade>=3)and(r<0.90)then
		r = 0.9
	end
	if(r<0.5)then
		return name.." (consumer)"
	elseif(r<0.75)then
		return name.." (industrial)"
	elseif(r<0.90)then
		return name.." (millitary)"
	else
		return name.." (space)"
	end
end

function select_item_name(tag_type, tag_name)
	--load item list and its probabilities
	local possible_item_list = xml.list(tag_type, tag_name, "item")
	local possible_item_prop = {}
	local inc
	possible_item_prop[1] = 0
	xml.push()
	for inc=1,#possible_item_list do 
		xml.load_node(possible_item_list[inc], "item", "name")
		local temp
		temp = xml.get_value("grade")
		if(temp~="-1")then
			temp = xml.get_value("probability")
			if(temp==nil)then
				possible_item_prop[inc+1] = possible_item_prop[inc]+1
			else
				local prop = tonumber(temp)
				possible_item_prop[inc+1] = possible_item_prop[inc]+prop*prop
			end
		else
			possible_item_prop[inc+1] = possible_item_prop[inc]--ignore this way
		end
	end
	xml.pop()
	local selected_item = ""
	--select an item from the list
	local prop = math.random()*possible_item_prop[#possible_item_prop]
	for inc=1,#possible_item_list do 
		if(prop<possible_item_prop[inc+1])then
			selected_item = possible_item_list[inc]
			break
		end
	end
	return selected_item
end


xml.load_node(object.get_name(), "character", "name")
--characteristics: equip items
local equip_types = {}
local equip_probabilities = {}
local equip_limbs = {}
local equip_tag = {}
local number_of_equips = xml.count("equip")
local limbs = {}
local temp
for i=0,number_of_equips-1 do
	xml.push()
	xml.load_node("equip", i)
	temp = xml.get_value("name")
	if(temp==nil)then
		equip_types[i+1] = ""
	else
		equip_types[i+1] = temp
	end
	temp = xml.get_value("tag")
	if(temp==nil)then
		equip_tag[i+1] = ""
	else
		equip_tag[i+1] = temp
	end
	temp = xml.get_value("probability")
	if(temp==nil)then
		equip_probabilities[i+1] = 1
	else
		equip_probabilities[i+1] = tonumber(temp)
	end
	equip_limbs[i+1] = xml.get_value("limb")
	local found = false
	for j=1,#limbs do
		if(limbs[j]==equip_limbs[i+1])then
			found = true
			break
		end
	end
	if(found==false)then
		limbs[#limbs+1] = equip_limbs[i+1]
	end
	xml.pop()
end
for j=1,#limbs do
	--create limb items and probability sum distribution
	local limb_items = {}
	local limb_tags = {}
	local limb_probabilities = {}
	limb_probabilities[1] = 0
	local current = 1
	for i=1,#equip_types do
		if(equip_limbs[i]==limbs[j])then
			limb_items[current] = equip_types[i]
			limb_tags[current] = equip_tag[i]
			limb_probabilities[current+1] = limb_probabilities[current]+equip_probabilities[i]
			current = current+1
		end
	end
	--choose an item for the limb
	local rnd = math.random()*limb_probabilities[#limb_probabilities]
	local choice_name = ""
	local choice_tag = ""
	for i=1,#limb_probabilities-1 do
		if(rnd<=limb_probabilities[i+1])then
			choice_name = limb_items[i]
			choice_tag = limb_tags[i]
			break
		end
	end
	if(choice_tag~="")and(choice_name~="")then
		choice_name = select_item_name(choice_name, choice_tag)
	end
	if(choice_name~="")then
		ai.create_limb_item(limbs[j], choice_name)
	end
end

--characteristics: inventory items
local item_types = {}
local item_probabilities = {}
local item_planets = {}
local item_grades = {}
local item_quantity = {}
local create_flag = {}
local always_flag = {}
local forbid_flag = {}
local tag_name = {}
local number_of_items = xml.count("random_item")
for i=0,number_of_items-1 do
	xml.push()
	xml.load_node("random_item", i)
	item_types[i+1] = xml.get_value("name")
	temp = xml.get_value("probability")
	if(temp==nil)then
		item_probabilities[i+1] = 1
	else
		item_probabilities[i+1] = tonumber(temp)
	end
	temp = xml.get_value("min_grade")
	if(temp==nil)then
		item_grades[i+1] = -1
	else
		item_grades[i+1] = tonumber(temp)
	end
	temp = xml.get_value("quantity")
	if(temp==nil)then
		item_quantity[i+1] = 1
	else
		item_quantity[i+1] = tonumber(temp)
	end
	temp = xml.get_value("tag")
	if(temp==nil)then
		tag_name[i+1] = ""
	else
		tag_name[i+1] = temp
	end
	temp = xml.get_value("planet")
	if(temp==nil)then
		item_planets[i+1] = ""
	else
		item_planets[i+1] = temp
	end
	temp = xml.get_value("create_flag")
	if(temp==nil)then
		create_flag[i+1] = ""
	else
		create_flag[i+1] = temp
	end
	temp = xml.get_value("always_when_flag")
	if(temp==nil)then
		always_flag[i+1] = ""
	else
		always_flag[i+1] = temp
	end
	temp = xml.get_value("forbid_flag")
	if(temp==nil)then
		forbid_flag[i+1] = ""
	else
		forbid_flag[i+1] = temp
	end
	xml.pop()
end
local flags = {}
for i=1,#item_types do
	--check flags
	local has_flag = false
	local has_not_flag = true
	if(always_flag[i]~="")then
		for j=1,#flags do
			if(flags[j]==always_flag[i])then
				has_flag = true
				break
			end
		end
	end
	if(forbid_flag[i]~="")then
		for j=1,#flags do
			if(flags[j]==forbid_flag[i])then
				has_not_flag = false
				break
			end
		end
	end
	if( (math.random()<=item_probabilities[i])or has_flag )and(has_not_flag)and((item_planets[i]=="")or(item_planets==planet.get_type()))then
		if(create_flag[i]~="")then
			flags[#flags+1] = create_flag[i]
		end
		--create i item here
		local selected_item = ""
		if(tag_name[i]=="")then
			selected_item = get_graded_item(item_types[i], item_grades[i])
		else
			selected_item = select_item_name(item_types[i], tag_name[i])
		end
		--generate the item copies
		if(selected_item~="")then
			for q=1,item_quantity[i] do
				ai.create_inventory_item(selected_item)
			end
		end
		--end item creation
	end
end