local t = {}

t.on_process = function()
	-- impossible to stun
	if(ai.get_stun()>0)then
		ai.set_stun(0)
	end
	
	-- make reload process faster
	if(ai.load_item("vehicle canon"))then
		local reload = item.get_reload()
		if(reload>0)then
			item.set_reload(reload+0.5)
		end
	end
end


t.on_death = function()
	ai.load_self()
	local x = object.get_x()
	local y = object.get_y()
	object.load_parent()
	if(object.is_flat())then
		object.create_projectile(x, y+5, 0, 0, "Large Explosion")
	end
	ai.vanish()
end

return t