local t = {}

t.on_listen = function()
	push()
		push()
		local has_translator = false
		local speaker_race = ""
		if(ai.load_speaker())then
			speaker_race = ai.get_race()
			if(has_translator==false)and(ai.load_item("floor limb"))and(item.get_name()=="Universal Translator")then
				has_translator = true
			end
			if(has_translator==false)and(ai.load_item("left hand"))and(item.get_name()=="Universal Translator")then
				has_translator = true
			end
			if(has_translator==false)and(ai.load_item("right hand"))and(item.get_name()=="Universal Translator")then
				has_translator = true
			end
		end
		pop()
	if(has_translator==false)and(ai.load_item("floor limb"))and(item.get_name()=="Universal Translator")then
		has_translator = true
	end
	if(has_translator==false)and(ai.load_item("left hand"))and(item.get_name()=="Universal Translator")then
		has_translator = true
	end
	if(has_translator==false)and(ai.load_item("right hand"))and(item.get_name()=="Universal Translator")then
		has_translator = true
	end
	if(ai.get_race()~=speaker_race)and(has_translator==false)then
		ai.add_dialog_option("niaou")
	end
	pop()
	dofile('data/characters/humanoid/general_script.lua|on_listen')
end

t.on_speak = function()
	push()
	push()
	local has_translator = false
	local player_race = ""
	if(ai.load_player())then
		speaker_race = ai.get_race()
		if(has_translator==false)and(ai.load_item("floor limb"))and(item.get_name()=="Universal Translator")then
			has_translator = true
		end
		if(has_translator==false)and(ai.load_item("left hand"))and(item.get_name()=="Universal Translator")then
			has_translator = true
		end
		if(has_translator==false)and(ai.load_item("right hand"))and(item.get_name()=="Universal Translator")then
			has_translator = true
		end
	end
	pop()
	if(has_translator==false)and(ai.load_item("floor limb"))and(item.get_name()=="Universal Translator")then
		has_translator = true
	end
	if(has_translator==false)and(ai.load_item("left hand"))and(item.get_name()=="Universal Translator")then
		has_translator = true
	end
	if(has_translator==false)and(ai.load_item("right hand"))and(item.get_name()=="Universal Translator")then
		has_translator = true
	end
	if(ai.get_race()~=player_race)and(has_translator==false)then
		local str = ai.get_speech_text()
		local res = string.gsub(str, "(%w+)", function(w) if(string.len(w)<4)then return "niar" else return string.rep("niaou", math.floor(string.len(w)/4)) end end)
		ai.set_speech_text(res)
	end
	pop()
	dofile('data/characters/humanoid/general_script.lua|on_speak')
end

t.on_death = function()
	ai.load_self()
	if(object.on_ground())then
		ai.start_animation("knee death fall")
		object.make_sound("data/characters/feline/death.ogg", 0, 0, 1)
	end
end

return t
