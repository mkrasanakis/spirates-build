local t = {}

t.on_select = function()
	ai.load_self()
	xml.load_node(object.get_name(), "character", "name")
	ai.push()
	ai.load_speaker()
	local credits = ai.get_credits()
	local has_ship = ai.load_ship()
	ai.load_self()
	--load recruit options
	local option_names = {}
	local option_quantity = {}
	local option_price = {}
	local number_of_options = xml.count("recruit")
	local temp
	for i=0,number_of_options-1 do
		xml.push()
		xml.load_node("recruit", i)
		temp = xml.get_value("name")
		if(temp==nil)then
			option_names[i+1] = ""
		else
			option_names[i+1] = temp
		end
		temp = xml.get_value("price")
		if(temp==nil)then
			option_price[i+1] = 0
		else
			option_price[i+1] = tonumber(temp)
		end
		temp = xml.get_value("quantity")
		if(temp==nil)then
			option_quantity[i+1] = 1
		else
			option_quantity[i+1] = tonumber(temp)
		end
		xml.pop()
	end
	--create recruit options
	local option_num = 0
	for i=1,#option_names do
		if(option_price[i]<=credits)then
			ai.add_dialog_option("recruit_option", "Recruit "..option_quantity[i].."x "..option_names[i].." for "..option_price[i].." $")
			ai.set_speech_option_attribute(option_num, "recruit_order_selection", i-1)
			option_num = option_num+1
		end
	end
	ai.pop()
	
	if(has_ship==false)then
		ai.set_reply("recruit_no_ship")
	elseif(#option_names==0)then
		ai.set_reply("recruit_no_option")
	end
end

return t