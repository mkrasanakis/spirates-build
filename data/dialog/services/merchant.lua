local t={}

--NOTE: TO ALTER MERCHANT BEHAVIOR YOU MUST ALSO ALTER THE data\dialog\services\trade_accept.lua FILE
--buy and sell rates are affected by prosperity to up to a fifth their initial price
t.on_select = function()
	push()
	ai.load_speaker()
	local speaker_faction = ai.get_faction()
	local prosperity = ai.get_prosperity()/4.0
	if(prosperity<0.1)then
		prosperity = 0.1
	end
	ai.set_trade_rate(prosperity)
	pop()
	if(ai.get_faction()==speaker_faction)then
		ai.set_trade_rate(prosperity*1.3)
	else
		ai.set_trade_rate(prosperity*2)
	end
	ai.trade_with_speaker()
end

return t