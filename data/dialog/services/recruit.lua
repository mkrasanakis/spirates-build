local t = {}

t.on_select = function()
	local i = ai.get_speech_attribute("recruit_order_selection")
	ai.push()
	--load appropriate things from speaker
	ai.load_speaker()
	ai.load_self()
	xml.load_node(object.get_name(), "character", "name")
	local name 
	local price
	local quantity
	local temp
	xml.load_node("recruit", i)
	temp = xml.get_value("name")
	if(temp==nil)then
		name = ""
	else
		name = temp
	end
	temp = xml.get_value("price")
	if(temp==nil)then
		price = 0
	else
		price = tonumber(temp)
	end
	temp = xml.get_value("quantity")
	if(temp==nil)then
		quantity = 1
	else
		quantity = tonumber(temp)
	end
	ai.add_credits(price)
	ai.pop()
	ai.load_self()
	--spawn characters here
	local x = 0
	local y = 0
	if(ai.load_ship()==false)then
		object.load_parent()
		x = object.get_x()
		y = object.get_y()
	else
		ship.load_self()
	end
	if(object.is_flat())then
		ai.add_credits(-price)
		for i=-math.floor(quantity/2),quantity-math.floor(quantity/2)-1 do
			local x_pos = x+math.random(-5,5)+i*10+5
			if(x_pos>object.get_size_x()/2-10)then
				x_pos = object.get_size_x()/2-10
			end
			if(x_pos<-object.get_size_x()/2+10)then
				x_pos = -object.get_size_x()/2+10
			end
			push()
			object.create_character(ai.get_faction(), x_pos, y, name)
			ai.set_alpha(0)
			pop()
		end
		if(ship.load_object())then
			ship.update_crew()
		end
		loop.message("Hired "..quantity.."x "..name)
	end
end

return t