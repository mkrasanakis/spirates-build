local t={}

t.on_select = function()
	local special = ""
	ai.push()
	if(ai.load_speaker())then
		if(ai.get_race()=="Cyborg")then
			special = "You are already a Cyborg."
		elseif(ai.get_race()~="Human")then
			special = "Only Humans can be come cyborgs."
		elseif(ai.get_credits()<5000)then
			special = "You don't have enough money."
		else
			ai.add_credits(-5000)
			ai.change_to("Enforcer")
		end
	end
	ai.pop()
	if(special~="")then
		ai.set_reply("doctor", special)
	end
end	

return t
