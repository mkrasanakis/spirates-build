local t={}

t.on_select = function()
	local diff = 0
	local ai_faction = ai.get_faction()
	local prosp = ai.get_prosperity()/4.0
	if(prosp<0.2)then
		prosp = 0.2
	end
	ai.push()
	if(ai.load_speaker())then
		ai.load_self()
		diff = object.get_max_health()-object.get_health()
		if(diff*10*prosp<=ai.get_credits())then
			object.add_health(diff+1)
		else
			diff = 0
		end
		ai.add_credits(-diff*10*prosp)
	end
	ai.pop()
	ai.add_credits(diff*10*prosp)	
end	

return t
