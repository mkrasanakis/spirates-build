local t={}

t.get_cost = function()
	local new_trade_rate = 0
	local speaker_faction = ai.get_faction()
	push()
	ai.load_speaker()
	local prosp = ai.get_prosperity()/4.0
	if(prosp==0)then prosp = 1
	elseif(prosp<0.1)then prosp = 0.1 end
	local new_trade_rate = 1
	if(simulation.difficulty()<1)then new_trade_rate = 0.5
	elseif(simulation.difficulty()<2)then new_trade_rate = 1
	elseif(simulation.difficulty()<3)then new_trade_rate = 1.5
	else new_trade_rate = 2 end
	if(prosp<=0.3)then prosp = 0.3 end
	if(ai.get_faction()~=speaker_faction)and(ai.get_relation(speaker_faction)<10)then
		new_trade_rate = new_trade_rate*2
	end
	new_trade_rate = new_trade_rate*prosp
	pop()
	local diff = 0
	if(ai.load_ship())then
		ship.load_self()
		diff = object.get_max_health()-object.get_health()
	end
	diff = math.ceil(diff*new_trade_rate/10)*1000
	return diff
end

t.on_create = function()
	local text = string.gsub(ai.get_speech_text(), "{cost}", ""..t.get_cost())
	ai.set_speech_text(text)
end

return t