local t={}

t.on_create = function()
	local diff = 0
	local prosp = ai.get_prosperity()/4.0
	if(prosp<0.2)then
		prosp = 0.1
	end
	ai.load_self()
	diff = object.get_max_health()-object.get_health()
	diff = math.ceil(diff*10*prosp)
	local text = string.gsub(ai.get_speech_text(), "{cost}", ""..diff)
	ai.set_speech_text(text)
end

return t