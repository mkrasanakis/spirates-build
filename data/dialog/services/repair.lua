local t={}

t.get_cost = function()
	local new_trade_rate = 0
	local speaker_faction = ai.get_faction()
	push()
	ai.load_speaker()
	local prosp = ai.get_prosperity()/4.0
	if(prosp<0.2)then
		prosp = 0.2
	end
	if(ai.get_faction()~=speaker_faction)then
		prosp = prosp*1.5
	end
	pop()
	local diff = 0
	if(ai.load_ship())then
		ship.load_self()
		diff = object.get_max_health()-object.get_health()
	end
	diff = math.ceil(diff*new_trade_rate/10)*1000
	return diff
end

--buy and sell rates are affected by prosperity to up to a fifth their initial price
t.on_select = function()
	local has_ship = false
	local diff = t.get_cost()
	local no_money = false
	ai.push()
	ai.load_speaker()
	if(ai.load_ship())then
		ship.load_self()
		diff = object.get_max_health()-object.get_health()
		if(ai.get_credits()>=diff)then
			object.add_health(object.get_max_health())
		else
			diff = 0
			no_money = true
		end
	end
	ai.add_credits(-diff)
	ai.pop()
	ai.add_credits(diff)
	if(has_ship==false)then
		ai.set_reply("repair_no_ship")
	elseif(no_money)then
		ai.set_reply("repair_no_money")
	else
		ai.set_reply("repair_no_cost")
	end
end

return t