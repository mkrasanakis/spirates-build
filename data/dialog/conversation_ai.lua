function perform_conversation()
	--load ai properties
	ai.load_self()
	local ai_mood = ai.get_attribute("mood")--mood >0 means positive attitude, if <0 negative attitude (mean abs should be kept arround 500)
	local ai_faction= ai.get_faction()
	local ai_id = object.get_index()
	local ai_x = object.get_x()
	local ai_y = object.get_y()
	local ai_size_x = object.get_size_x()
	local ai_size_y = object.get_size_y()
	local ai_speech = ai.get_speech()
	local ai_listen = ai.get_listen()
	local spoken = false
	local end_speech = false
	--if outside conversation get conversation target
	if(ai.is_speaking()==false)then --[[
		--get speach target
		ai.load_self()
		ai.push()
		object.load_parent()
		local target = -1
		local n = object.get_number_of_children()
		local max_value = 0
		local i
		for i = 0, n-1 do
			object.load_child(i)
				if(i~=ai_id)and(ai.load_object())and(ai.is_dead()==false)and(ai.get_speech()=="")and(ai.get_listen()=="")then
					value = 1.0/(1+math.abs(ai_x-object.get_x()))
					if(value>max_value)then
						max_value = value
						target = i
					end
				end
			object.load_parent()
		end
		ai.pop()
		--speak to target
		spoken = (target~=-1)
		if(spoken)then
			ai.load_self()
			object.load_parent()
			object.load_child(target)
			ai.speak_to_object("hello","Hello!")
		end
	--if speaking chose an option
	--]]
	else
		--information of speaker
		local speaker_faction = ""
		local speaker_rank    = 10
		local speaker_reward  = 0
		local speaker_health  = 0
		local speaker_bounty  = 0
		ai.push()
		if(ai.load_speaker())then
			speaker_faction = ai.get_faction()
			speaker_rank    = ai.get_rank()
			speaker_reward  = ai.get_reward()
			speaker_health  = ai.get_health()
			speaker_bounty  = ai.get_bounty(ai.get_faction())
		end
		ai.pop()
		local capability_fraction = (math.abs(ai.get_health())+1)/(math.abs(speaker_health)+1)
		--select a random option to give more weight
		local n = ai.get_speech_option_number()
		local choice = -1
		if(n>0)then
			local random_choice = math.random(0, n-1)
			local i
			local max_value = -10
			local value
			--get max value for each option
			for i = 0, n-1 do
				value = 0
				extra_value = 0
				value = 0
				--create attributes
				value = value + ai.get_speech_option_attribute(i, "weight")
				if(ai.get_faction()~=speaker_faction)then
					value = value + ai.get_speech_option_attribute(i, "weight_per_relation")*ai.get_relation(speaker_faction)
				else
					value = value + ai.get_speech_option_attribute(i, "weight_per_same_faction")
				end
				value = value + ai.get_speech_option_attribute(i, "weight_per_thousand_mood")*ai.get_attribute("mood")/1000
				value = value + ai.get_speech_option_attribute(i, "weight_per_thousand_credits")*ai.get_credits()/1000
				value = value + ai.get_speech_option_attribute(i, "weight_per_rank")*ai.get_rank()
				if(ai.get_max_health()>0)then
					value = value + ai.get_speech_option_attribute(i, "weight_per_health_frac")*ai.get_health()/ai.get_max_health()
				end
				value = value + ai.get_speech_option_attribute(i, "weight_per_thousand_bounty")*ai.get_bounty(ai.get_faction())
				value = value + ai.get_speech_option_attribute(i, "weight_per_thousand_speaker_bounty")*ai.get_bounty(speaker_faction)
				value = value + ai.get_speech_option_attribute(i, "weight_add_random")*math.random()
				--keep only if max value
				if(value>max_value)then
					max_value = value
					choice = i
				end
			end
		end
		--
		spoken = (choice~=-1)
		if(spoken)then
			ai.select_speech_option(choice)
		else
			end_speech = true
		end
	end

	--if spoken reset speech timer
	if(spoken)then
		ai.push()
		if(ai.load_speaker())then
			ai.set_attribute("speech_timer", 0)
		end
		ai.pop()
	end

	if(end_speech)then
		ai.set_attribute("speech_timer", -200)
		ai.push()
		if(ai.load_speaker())then
			ai.set_attribute("speech_timer", -200)
		end
		ai.pop()
		ai.end_speech()
	end
	return
end