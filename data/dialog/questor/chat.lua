local t = {}

function table.contains(element, table)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

t.on_create = function()
	local text = ai.get_speech_text()
	text = t.edit_text(text)
	--correct upper cases
	--set the sentence to speak
	ai.set_speech_text(text)
end

t.edit_text = function(text)
	--get sentence
	
	local problem_category = {
		{"left me",{1,2}},
		{"became estranged",{1,2}},
		{"doesn't want to see me",{2}},
		{"died in a raid",{1,2}},
		{"joined the city guard",{2,4}},
		{"became a pirate",{1,2,4}},
		{"joined the rebels",{2,4}},
		{"performed the UMMI surgery and became an enforcer",{2,4}},
		{"bought a robot exoskeleton for battle",{2,4}},
		{"took over a town",{2,4}},
		{"job",{3}},
		{"work",{3}},
		{"corporation",{3}},
		{"want to drink",{4}}
	}
	local problem_sentiment = {
		{"",{1,2,3,4}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"",{1,2,3}},
		{"ruddy ",{1,2,3}},
		{"blasted ",{1,2,3}},
		{"cute ",{1,2}},
		{"beloved ",{1,2,3}},
		{"brilliant ",{2}},
		{"brilliantly ",{3}},
		{"regrettably ",{3}}
	}
	--source second parameter will NEVER be greater to category second parameter
	local problem_source = {
		{'my {sentiment}child',2},
		{'my {sentiment}partner',2},
		{'my {sentiment}best friend',2},
		{'my {sentiment}parent',2},
		{'my {sentiment}cat',1},
		{'I {sentiment}lost',3},
		{'I',4}
	}
	--achievements
	local achievements = {
		{'graduated from university'},
		{'invented a new {item1}'},
		{'was gifteed with a {item1}'},
		{'engineered a {item1}'},
		{'married'}
	}
	--begs
	local begs = {
		{'Buy a {item1}'},
		{'Please buy a {item1}'}
	}
	--begs
	local charity = {
		{'bread cancer victims', 500},
		{'cyborgs who miss their luck', 500},
		{'victims of Nanite poisoning', 500},
		{'rehabilitation of UMMI guards', 500}
	}
	--items with prices
	local items = {
		{'Diesel Generator', 500},
		{'Headband (blue)', 500},
		{'Baseball Hat (blue)', 500},
		{'Martial Belt (white)', 500},
		{'Straw Hat', 500},
		{'Shirt', 500},
		{'Box', 500},
		{'Jet Pack', 1000},
		{'Antigravity Generator', 1500},
		{'Energy Shield', 1500},
		{'Wind Generator', 800},
		{'Solar Panel', 800},
		{'Stun Baton', 750},
		{'Escape Capsule', 1000},
		{'Titan Exoskeleton', 5000},
		{'Fried Chicken', 100},
		{'Bread', 100},
		{'Wine', 100},
		{'Beer', 100},
		{'Milk', 100},
		{'Fish', 100},
		{'Grenade Launcher (consumer)', 750}
	}
	local i
	--find problems
	i = 1
	while(string.find(text, '{problem'..i..'}'))do
		local problem_i_category = ai.get_attribute('character_problem'..i..'_category')
		local problem_i_source = ai.get_attribute('character_problem'..i..'_source')
		local problem_i_sentiment = ai.get_attribute('character_problem'..i..'_sentiment')
		if(problem_i_category==0)then
			problem_i_category = math.random(1, #problem_category)
			problem_i_source = math.random(1, #problem_source)
			problem_i_sentiment = math.random(1, #problem_sentiment)
			while(table.contains(problem_source[problem_i_source][2], problem_category[problem_i_category][2])==false)do
				problem_i_category = math.random(1, #problem_category)
			end
			while(table.contains(problem_source[problem_i_source][2], problem_sentiment[problem_i_sentiment][2])==false)do
				problem_i_sentiment = math.random(1, #problem_sentiment)
			end
			ai.set_attribute('character_problem'..i..'_category', problem_i_category)
			ai.set_attribute('character_problem'..i..'_source', problem_i_source)
			ai.set_attribute('character_problem'..i..'_sentiment', problem_i_sentiment)
		end
		local src = problem_source[problem_i_source][1]..' '..problem_category[problem_i_category][1]
		text = string.gsub(text, '{problem'..i..'}', src)
		text = string.gsub(text, '{sentiment}', problem_sentiment[problem_i_sentiment][1])
		i = i+1
	end
	
	i = 1
	while(string.find(text, '{achievement'..i..'}'))do
		local achievement_i = ai.get_attribute('character_achievement'..i)
		if(achievement_i==0)then
			achievement_i = math.random(1, #achievements)
			ai.set_attribute('character_achievement'..i, achievement_i)
		end
		text = string.gsub(text, '{achievement'..i..'}', achievements[achievement_i][1])
		i = i+1
	end
	
	i = 1
	while(string.find(text, '{beggar'..i..'}'))do
		local beg_i = ai.get_attribute('character_beggar'..i)
		if(beg_i==0)then
			beg_i = math.random(1, #begs)
			ai.set_attribute('character_beggar'..i, beg_i)
		end
		text = string.gsub(text, '{beggar'..i..'}', begs[beg_i][1])
		i = i+1
	end
	
	i = 1
	while(string.find(text, '{item'..i..'}'))do
		local item_i = ai.get_attribute('conversation_item'..i)
		if(item_i==0)then
			item_i = math.random(1, #items)
			ai.set_attribute('conversation_item'..i, item_i)
			ai.create_inventory_item(items[item_i][1])
		end
		text = string.gsub(text, '{item'..i..'}', items[item_i][1])
		i = i+1
	end
	i = 1
	while(string.find(text, '{item_price'..i..'}'))do
		local item_i = ai.get_attribute('conversation_item'..i)
		if(item_i==0)then
			item_i = math.random(1, #items)
			ai.set_attribute('conversation_item'..i, item_i)
			ai.create_inventory_item(items[item_i][1])
		end
		text = string.gsub(text, '{item_price'..i..'}', items[item_i][2])
		i = i+1
	end
	
	i = 1
	while(string.find(text, '{charity'..i..'}'))do
		local charity_i = ai.get_attribute('character_charity'..i)
		if(charity_i==0)then
			charity_i = math.random(1, #charity)
			ai.set_attribute('character_charity'..i, charity_i)
		end
		text = string.gsub(text, '{charity'..i..'}', charity[charity_i][1])
		i = i+1
	end
	i = 1
	while(string.find(text, '{charity_price'..i..'}'))do
		local charity_i = ai.get_attribute('character_charity'..i)
		if(charity_i==0)then
			charity_i = math.random(1, #charity)
			ai.set_attribute('character_charity'..i, charity_i)
		end
		text = string.gsub(text, '{charity_price'..i..'}', charity[charity_i][2])
		i = i+1
	end
	
	return text
end

return t