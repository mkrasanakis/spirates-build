function talk_about_hate(faction, relation)
	local r = math.random()
	local text = ""
	if(relation>0)then
		text = "The "..faction.." are good friends to us."
	elseif(relation>=-1)then
		if(r<0.33)then
			text = "The "..faction.." are to be wary of."
		elseif(r<0.66)then
			text = "Be wary of the "..faction..", as they donnot have the proper behaviour towards us."
		else
			text = "There has been a minor incident lately by the "..faction.."."
		end
	elseif(relation>=-3)then
		if(r<0.33)then
			text = "The "..faction.." are enemies to oppose."
		elseif(r<0.66)then
			text = "We must not forget that the "..faction.." has caused us mischief."
		else
			text = "Be wary of the "..faction..", as it has done things against us."
		end
	else
		if(r<0.33)then
			text = "The "..faction.." are our hateful enemies."
		elseif(r<0.66)then
			text = "The hateful "..faction.." has caused us grief and anger."
		else
			text = "We must not forget that the "..faction.." are our sworn enemies."
		end
	end

	return text
end