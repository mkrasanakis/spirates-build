function talk_about_danger(faction, relative_bounty)
	local text = "The "..faction.." ";
	local r = math.random()
	if(r<0.2)then
		text = text.."has risen in infamy lately."
	elseif(r<0.4)then
		text = text.."has become notorious lately."
	elseif(r<0.6)then
		text = text.."has become a source of fear for people."
	elseif(r<0.8)then
		text = text.."is being feared."
	else
		text = text.."is the top in the most wanted list."
	end
		
	if(ai.get_bounty()>10000)then
		if(relative_bounty<-50000)then
			text = text.." But they are not even close to out level yet."
		elseif(relative_bounty<-25000)then
			text = text.." And they are starting to become as well known as us."
		elseif(relative_bounty<-5000)then
			text = text.." And they have almost reached our level."
		elseif(relative_bounty<5000)then
			text = text.." And they are on par with us."
		elseif(relative_bounty<25000)then
			text = text.." They are even more widely known than us."
		elseif(relative_bounty<50000)then
			text = text.." We are lagging behind them."
		else
			text = text.." We are no match for them."
		end
	end
	
	return text
end