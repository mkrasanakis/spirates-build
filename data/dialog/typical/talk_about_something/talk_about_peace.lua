function talk_about_peace(peace_loving)
	local text = ""
	if(peace_loving>0)then
		local r = (math.random()*peace_loving+peace_loving)/2
		if(r>400)then
			text = "It would be great if peaceful days lasted!"
		elseif(r>300)then
			text = "It's all peaceful lately and that's really great!"
		elseif(r>200)then
			text = "I like it when it's peaceful."
		elseif(r>100)then
			text = "Everything is calm lately."
		else
			text = "No threats have appeared lately."
		end
	else
		local r = (math.random()*(-peace_loving)-peace_loving)/2
		if(r<100)then
			text = "No danger has appeared lately."
		elseif(r<200)then
			text = "I am bored with doing nothing."
		elseif(r<300)then
			text = "It's too nice and quiet with nothing happening lately."
		elseif(r<400)then
			text = "I long for days of danger and action."
		elseif(r<500)then
			text = "I wish some clash will happen to end this standstill."
		else
			text = "I want to hack and slash and shoot and do anything to destroy something of this peaceful environment!"
		end
	end
	return text
end