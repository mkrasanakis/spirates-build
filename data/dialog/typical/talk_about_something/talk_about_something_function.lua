require("data/dialog/typical/talk_about_something/talk_about_danger_and_hate")
require("data/dialog/typical/talk_about_something/talk_about_danger")
require("data/dialog/typical/talk_about_something/talk_about_hate")
require("data/dialog/typical/talk_about_something/talk_about_peace")

function talk_about_something()
	local faction_with_most_bounty = ""
	local most_bounty = 0
	local faction_with_worst_relation = ""
	local worst_relation = 0

	local n = ai.get_number_of_known_factions()
	local i
	for i=0,n-1 do
		local bounty   = ai.get_known_faction_bounty(i)
		local relation = ai.get_known_faction_relation(i)
		if(relation<worst_relation)then
			worst_relation = relation
			faction_with_worst_relation = ai.get_known_faction(i)
		end
		if(bounty>most_bounty)then
			most_bounty = bounty
			faction_with_most_bounty = ai.get_known_faction(i)
		end
	end

	local text = ""
	if(math.random()<0.25)then
		if(worst_relation<-1-most_bounty/1000)then
			text = talk_about_hate(faction_with_worst_relation, worst_relation)
		elseif(most_bounty>0)then
			text = talk_about_danger(faction_with_most_bounty, most_bounty-ai.get_bounty())
		else
			text = talk_about_peace(-ai.get_bounty(ai.get_faction())+ai.get_attribute("mood"))
		end
	end
end