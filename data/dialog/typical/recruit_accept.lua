local t={}

t.on_select = function()
	ai.push()
	if(ai.load_speaker())then
		local speaker_faction = ai.get_faction();
		ai.pop()
		ai.set_faction(speaker_faction)
	else
		ai.pop()
	end
end

return t