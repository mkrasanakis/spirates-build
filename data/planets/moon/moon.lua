tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating moon",(global_current_planet)/global_max_planet)
--initialize planet
local r = planet.get_r()
sizes = r/100
planet.set_table_sizes(sizes)
for i = 0, planet.get_table_sizes()-1 do
	planet.set_bg_ground_r(r*0.9,i)
end
tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Moon generated",(global_current_planet)/global_max_planet)
