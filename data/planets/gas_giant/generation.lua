require('data/planets/generate_terrestrial_planet')

local available_towns = {"Pirate Town", "Scrapyard Town", "Robot Production"}
local available_ships = {"Pirate Raider", "Pirate Warmaker", "Rebel Adventurer", "Rebel Transport"}
local available_wreckage = {}
local available_fish = {}
local available_decorations = {}
local number_of_ships = 20

object.set_alias(ai.generate_name("Planet Gas Giant"))
generate_terrestrial_planet(available_towns, available_ships, number_of_ships, available_wreckage, available_fish, available_decorations)

planet.set_spin_period(planet.get_spin_period()*(math.random()+1))