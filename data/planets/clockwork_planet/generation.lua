require('data/planets/generate_terrestrial_planet')

local available_towns = {"UMMI Town"}
local available_ships = {"UMMI Patrol"}
local available_wreckage = {}
local available_fish = {}
local available_decorations = {}
local number_of_ships = 10

object.set_alias(ai.generate_name("Planet Clockwork"))
generate_terrestrial_planet(available_towns, available_ships, number_of_ships, available_wreckage, available_fish, available_decorations)

planet.set_spin_period(planet.get_spin_period()*(math.random()+1))