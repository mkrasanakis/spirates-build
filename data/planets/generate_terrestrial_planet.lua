-- Generates a terrestrial planet with the appropriate possible towns that are available for it.
-- The probability of placing each town is the same, so you may need to have multiple copies of the same town
-- to increase its probability of spawning.

function generate_terrestrial_planet(available_towns, available_ships, number_of_ships, available_wreckage, sea_creatures, decorations, number_of_towns)
	local i,j
	local r = planet.get_r()
	local sizes = r/2
	local towns_per_size = 0.05
	local max_towns = 20
	if(number_of_towns==nil)then
		number_of_towns = math.min(sizes*towns_per_size, max_towns)
	end
	local town_distance = sizes/number_of_towns
	local size_per_size = 2*r*math.cos(3.14/2 - 3.14/sizes)
	planet.set_table_sizes(sizes,sizes/2,sizes,sizes)
	planet.load_self()
	local town_over_water = 20/r--00035

	--tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Initializing planet",((global_current_planet-1)+0)/global_max_planet)
	w_r = planet.get_water_r(0)
	--generate terrain
	tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating terrain and towns",((global_current_planet-1)+0.1)/global_max_planet)
	
	local j = 0;
	local i = math.random(10,20);
	while j < number_of_towns and i < sizes do
		local i_r = w_r+town_over_water*r
		local theta = 360.0*(i-0.5)/planet.get_table_sizes()
		local selection = math.random(1, #available_towns)
		local relative_size = math.random(0.8,1.2)
		local point_size = i_r/planet.get_table_sizes()*3.141592*2
		planet.create_town(i_r, theta, relative_size, available_towns[selection])
		local factor = planet.get_table_sizes()/(2*3.141592*object.get_r())
		local city_size = math.ceil(object.get_size_x()/2*factor)
		object.set_size_x(city_size*2/factor+5)
		for k=i-city_size,i+city_size do
			planet.set_bg_ground_r(i_r+5+town_over_water*r*(1-math.cos((k-i)/planet.get_table_sizes()*3.141592)),k)
		end
		j = j + 1;
		i = i + city_size*2 + (math.random() + 1)*town_distance
	end

	for i=0,sizes-1 do
		local i_r = planet.get_bg_ground_r(i)
		if i_r < w_r then
			planet.set_bg_ground_r(0.99*w_r,i)
		end
	end
--[[
	for i=0,sizes-1 do
		local i_r = planet.get_bg_ground_r(i)
		if i_r < w_r then
			planet.set_bg_ground_r(w_r,i)
		local i_r1 = planet.get_bg_ground_r(i-1)
		local i_r2 = planet.get_bg_ground_r(i+1)
		
		planet.set_bg_ground_r((i_r1 + i_r2)/2,i);
		end
	end
	
	for i=sizes-1,0,-1 do
		local i_r = planet.get_bg_ground_r(i)
		if i_r < w_r then
			planet.set_bg_ground_r(w_r,i)
		local i_r1 = planet.get_bg_ground_r(i-1)
		local i_r2 = planet.get_bg_ground_r(i+1)
		
		planet.set_bg_ground_r((i_r1 + i_r2)/2,i);
		end
	end
--]]
	
	tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating ships",((global_current_planet-1)+0.3)/global_max_planet)
	--generate some ships
	if(number_of_ships>0)then
		local step = 360/number_of_ships
		local step_std = math.floor(step/2)
		local center = math.random(step-step_std, step+step_std)
		while(center<360)do
			local ship_pos = center/360*planet.get_table_sizes()
			planet.create_ship_mob(planet.get_water_r(ship_pos), center, available_ships[math.random(1, #available_ships)])	
			center = center + math.random(step-step_std, step+step_std)
		end
	end
	
---[[
	tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating shipwrecks",((global_current_planet-1)+0.5)/global_max_planet)
	--generating wreckage
	if(number_of_ships>0)and(available_wreckage)and(#available_wreckage>=1)then
		number_of_ships = number_of_ships/2+1
		local step = 360/number_of_ships
		local step_std = math.floor(step/2)
		local center = math.random(step-step_std, step+step_std)
		while(center<360)do
			local ship_pos = center/360*planet.get_table_sizes()
			planet.create_ship(planet.get_fg_ground_r(ship_pos), center, available_wreckage[math.random(1, #available_wreckage)])	
			object.add_health(-object.get_max_health()-1)
			center = center + math.random(step-step_std, step+step_std) + 5
		end
	end
--]]


--[[
	tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating shipwrecks",((global_current_planet-1)+0.5)/global_max_planet)
	--generating wreckage
	local number_of_wreckage = 50
	if(number_of_wreckage>=1)then
		local step = 360/number_of_wreckage
		local step_std = math.floor(step/2)
		local center = math.random(step-step_std, step+step_std)
		while(center<360)do
			local ship_pos = center/360*planet.get_table_sizes()
			planet.create_ship_mob(planet.get_fg_ground_r(ship_pos), center, available_ships[math.random(1, #available_ships)])	
			ship.sink()
			center = center + math.random(step-step_std, step+step_std) + 5
		end
	end
--]]

--[
	if(#sea_creatures>0)then
		tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating sea creatures",((global_current_planet-1)+0.6)/global_max_planet)
		--generating whales (number less than whale_rarity)
		local whale_rarity = 5
		for center = planet.get_table_sizes()/whale_rarity*math.random(), planet.get_table_sizes(), planet.get_table_sizes()/whale_rarity*(1+math.random()) do
			local ang = center*2*3.14159/planet.get_table_sizes()
			planet.load_self()
			local r = w_r-math.random()*50
			object.create_character(r*math.cos(ang), r*math.sin(ang), sea_creatures[math.random(1,#sea_creatures)])
		end
	end
--]


---[[
	--generate planet clouds in clusters
	tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating clouds",((global_current_planet-1)+0.7)/global_max_planet)
	if(number_of_ships>=0)then
		cloud_textures = {}
		cloud_textures[0] = "cloud0"
		cloud_textures[1] = "cloud1"
		cloud_textures[2] = "cloud2"
		for center = 0, planet.get_table_sizes(), (150+math.random(150))*5 do
			--get a random height for clouds
			center_r = planet.get_water_r(0) + (150 + 50*math.random())
			--for each individual cluster on designated center, create some clouds at random height
			num = (10+math.random(10))/2
			for num_loop = 0, num do
				pos = center + (math.random(60)-30)*5
				offset_r = center_r + 0.005*math.random()*r
				texture = cloud_textures[math.random(0,2)];
				planet.new_cloud(offset_r, pos*360/planet.get_table_sizes(), texture)
			end
		end
	end
--]]

--[[
	--generate planet decorations
	tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Generating sea decorations",((global_current_planet-1)+0.8)/global_max_planet)
	if(number_of_ships>=0)and(#decorations>0)then
		center_r = planet.get_fg_ground_r(0)
		for center = 0, 2*3.14159*center_r, 60+math.random(120) do
			pos = center-60
			while pos<=center+60 do
				texture = decorations[math.random(1,#decorations)]
				planet.new_decoration(center_r, pos*360/center_r/2/3.14159, texture, 0.8+math.random()*0.4)
				pos = pos + math.random()*30+30
			end
		end
	end
--]]

	--create waves
	--tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Applying waves",((global_current_planet-1)+0.9)/global_max_planet)
	for i = 0, planet.get_table_sizes(), 10 do
		planet.set_water_r(planet.get_water_r(i)+math.random()*25-12, i)
	end

	--finished
	--tell_progress("System planet #"..global_current_planet.."/"..global_max_planet..": Finished planet generation",(global_current_planet)/global_max_planet)
end
