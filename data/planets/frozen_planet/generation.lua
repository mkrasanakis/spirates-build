require('data/planets/generate_terrestrial_planet')

local available_towns = {"Pirate Town", "Ice Town", "Ice Town", "Ice Town"}
local available_ships = {"Rebel Adventurer"}
local available_wreckage = {}
local available_fish = {"Whale"}
local available_decorations = {"seaweed0", "stone", "coral", "stone"}
local number_of_ships = 20

object.set_alias(ai.generate_name("Planet Frozen"))
generate_terrestrial_planet(available_towns, available_ships, number_of_ships, available_wreckage, available_fish, available_decorations)

planet.set_spin_period(planet.get_spin_period()*(math.random()+1))