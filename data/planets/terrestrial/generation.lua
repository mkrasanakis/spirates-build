require('data/planets/generate_terrestrial_planet')

local available_towns = {"Pirate Town", "Pirate Town", "Casino Town", "UMMI Town", "UMMI Town", "UMMI Town"}
local available_ships = {"Pirate Raider", "Pirate Pillager", "Pirate Warmaker", "Pirate Escapist", "UMMI Patrol", "UMMI Tanker"}
local available_wreckage = {"Caravel", "Raft"}
local available_fish = {"Whale"}
local available_decorations = {"seaweed0", "seaweed0", "coral", "stone"}
local number_of_ships = 15

object.set_alias(ai.generate_name("Planet Terrestrial"))
generate_terrestrial_planet(available_towns, available_ships, number_of_ships, available_wreckage, available_fish, available_decorations)

planet.set_spin_period(planet.get_spin_period()*(math.random()+1))