require('data/planets/generate_terrestrial_planet')

local available_towns = {"Pirate Town", "Rebel Town", "Desert Town", "Desert Town"}
local available_ships = {"Pirate Raider", "Pirate Pillager", "Pirate Warmaker", "Feline Traveller", "Feline Seafarer", "Rebel Transport"}
local available_wreckage = {"Caravel", "Raft"}
local available_fish = {}
local available_decorations = {"stone"}
local number_of_ships = 10

object.set_alias(ai.generate_name("Planet Terrestrial"))
generate_terrestrial_planet(available_towns, available_ships, number_of_ships, available_wreckage, available_fish, available_decorations)

planet.set_spin_period(planet.get_spin_period()*(math.random()+1))