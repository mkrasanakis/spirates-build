uniform sampler2D light_texture;
uniform sampler2D texture;
uniform vec2 screen_size;

void main() 
{
	vec4 light = texture2D(light_texture, gl_FragCoord.xy/screen_size);
	vec4 pixel = texture2D(texture, vec2(gl_TexCoord[0]))*gl_Color;
	light.a = light.a + 0.1;
	
    gl_FragColor = light*pixel;
}
