uniform sampler2D texture;

void main() 
{
	float alpha = texture2D(texture,gl_TexCoord[0].xy).a;
	if(alpha < 0.1)
		discard;
    gl_FragColor = vec4(0,0,0,0);
	gl_FragDepth = gl_FragCoord.z;
}
