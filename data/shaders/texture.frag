uniform sampler2D texture;
uniform vec4 primary_color;
uniform vec4 secondary_color;

void main() 
{
	vec4 tex = texture2D(texture, vec2(gl_TexCoord[0]));
	gl_FragColor = tex*primary_color;
}
