uniform sampler2D texture;
uniform sampler2D back_texture;
uniform sampler2D light_texture;
uniform float time;
uniform vec2 screen_size;
uniform float blur_radius;
uniform int opaque;
uniform vec4 primary_color;
uniform vec2 screen_space_center;

void main() 
{	
	vec2 c0 = screen_space_center;
	vec2 texcoord = vec2(gl_TexCoord[0]);
	vec2 pos = texcoord - vec2(0.5,0.5);
	vec2 back_pos = gl_FragCoord.xy/screen_size;
	float r_ = 2.*length(pos);
	vec2 b = (back_pos - c0);
	float th = atan(pos.y,pos.x);

	float screen_r = length(b);
	float screen_R = screen_r/r_;
	float dr = screen_R;
	vec2 th_vec = vec2(cos(th) ,sin(th));

	vec4 water_pixel = texture2D(texture, texcoord);
	vec4 light = texture2D(light_texture, gl_FragCoord.xy/screen_size);
	light.a = 1.;
	
	gl_FragColor = water_pixel*primary_color*light;
	if(opaque == 0)
	{
		vec2 refl_pos = c0 + b/length(b)*dr;
		vec4 refl_pixel = texture2D(back_texture, refl_pos);
		if(refl_pos.y >= 0. && refl_pos.y <= 1.)
		{
			float a = 0.10*(r_ < 0.99?r_:99.*(r_ - 0.98));
			a = a >= 0.? a : 0.;
			gl_FragColor = a*refl_pixel + (1.-a)*gl_FragColor;
		}
	}

	gl_FragColor = gl_FragColor*r_;

	if(opaque != 0)
		gl_FragColor.a = 1.;
	else
		gl_FragColor.a = water_pixel.a;
}
