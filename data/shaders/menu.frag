uniform sampler2D texture;
uniform vec2 screen_size;

void main()
{
	vec2 pos = gl_FragCoord.xy/screen_size;
	
	vec4 pixel = texture2D(texture, pos);
	
	gl_FragColor = 0.4*pixel;
	gl_FragColor.a = 1.;
}
