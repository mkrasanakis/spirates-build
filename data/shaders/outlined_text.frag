uniform sampler2D texture;
uniform vec4 primary_color;
uniform vec4 secondary_color;

void main() 
{
	vec4 tex = texture2D(texture, vec2(gl_TexCoord[0]));
	if(tex.a <= 0.)
		discard;
	tex.a = 1.;
	gl_FragColor = tex*primary_color;
}
