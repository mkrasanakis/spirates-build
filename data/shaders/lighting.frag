uniform sampler2D light_texture;
uniform sampler2D texture;
uniform vec2 screen_size;
uniform vec4 primary_color;
uniform vec4 secondary_color;

void main() 
{
	vec4 tex = texture2D(texture, vec2(gl_TexCoord[0]));
	vec4 light = texture2D(light_texture, gl_FragCoord.xy/screen_size);
	
	light.a = 1.;
	gl_FragColor = light*tex*primary_color;
}
