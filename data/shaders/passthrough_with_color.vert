attribute vec2 vertex;
attribute vec2 texcoord;
attribute vec4 color;

uniform mat4 mvp;
uniform vec2 screen_size;
uniform float z;

void main( void )
{
    gl_Position = mvp * vec4(vertex.x,vertex.y,z,1);
    gl_TexCoord[0].xy = texcoord;
    gl_FrontColor = color;   
}
