uniform sampler2D texture;

void main()
{
	vec2 texcoord = vec2(gl_TexCoord[0]);
	vec4 pixel = texture2D(texture, texcoord);
	float x = 0.21*pixel.r + 0.71*pixel.g + 0.07*pixel.b ;
	gl_FragColor = vec4(x,x,x,1);
}
