uniform float time;
uniform sampler2D noise;
uniform sampler2D texture;
uniform vec4 primary_color;

void main() 
{
	vec2 texcoord = vec2(gl_TexCoord[0]);
	vec2 pos = texcoord - vec2(0.5,0.5);
	texcoord = texcoord + vec2(0.2*sin(0.00025*time), 0.2*sin(0.00025*time));
    float r = 2.*length(pos);
	
	float x = 1.;
	float s = 0;
	if(r > s)
	{
		float n = 0.5*texture2D(texture, vec2(0.025*time,0.0025*time)).r;	
		x = (x - (r-s)/(1.-s)) + 0.01*sin(0.0025*time + 0.5*n);
	}
	gl_FragColor = texture2D(texture, texcoord)*vec4(primary_color.x + x,primary_color.y + x,primary_color.z + x, x*primary_color.w);
}
