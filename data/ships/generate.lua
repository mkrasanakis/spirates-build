--xml.load_node(object.get_name(), "ship", "name")
--characteristics: faction
local faction_type = xml.get_value("faction_type")
local faction_name = xml.get_value("faction_name")
local min_guards = xml.get_value("min_crew")
local temp
if(min_guards==nil)then
	min_guards = 0
else
	min_guards = tonumber(min_guards)
end
local max_guards = xml.get_value("max_crew")
if(max_guards==nil)then
	max_guards = ship.get_capacity()/2-1
else
	max_guards = tonumber(max_guards)
end
if(max_guards<min_guards)then
	max_guards = min_guards
end
local bounty = xml.get_value("bounty")
if(bounty==nil)then
	bounty = 0
else
	bounty = tonumber(bounty)
end
--characteristics: crew
local guard_types = {}
local guard_probabilities = {}
local number_of_guards = xml.count("crew")
for i=0,number_of_guards-1 do
	xml.push()
	xml.load_node("crew", i)
	guard_types[i+1] = xml.get_value("name")
	temp = xml.get_value("probability")
	if(temp==nil)then
		guard_probabilities[i+1] = 1
	else
		guard_probabilities[i+1] = tonumber(temp)
	end
	xml.pop()
end
--characteristics: captain
local captain_types = {}
local captain_probabilities = {}
local number_of_captains = xml.count("captain")
for i=0,number_of_captains-1 do
	xml.push()
	xml.load_node("captain", i)
	captain_types[i+1] = xml.get_value("name")
	temp = xml.get_value("probability")
	if(temp==nil)then
		captain_probabilities[i+1] = 1
	else
		captain_probabilities[i+1] = tonumber(temp)
	end
	xml.pop()
end
--characteristics: equip
local item_types = {}
local item_quantity = {}
local item_probabilities = {}
local item_create_flag = {}
local item_require_flag = {}
local item_forbid_flag = {}
local number_of_items = xml.count("equip")
for i=0,number_of_items-1 do
	xml.push()
	xml.load_node("equip", i)
	item_types[i+1] = xml.get_value("name")
	temp = xml.get_value("quantity")
	if(temp==nil)then
		item_quantity[i+1] = 1
	else
		item_quantity[i+1] = tonumber(temp)
	end
	temp = xml.get_value("probability")
	if(temp==nil)then
		item_probabilities[i+1] = 1
	else
		item_probabilities[i+1] = tonumber(temp)
	end
	temp = xml.get_value("create_flag")
	if(temp==nil)then
		item_create_flag[i+1] = ""
	else
		item_create_flag[i+1] = xml.get_value("create_flag")
	end
	temp = xml.get_value("requires_flag")
	if(temp==nil)then
		item_require_flag[i+1] = ""
	else
		item_require_flag[i+1] = xml.get_value("create_flag")
	end
	temp = xml.get_value("forbids_flag")
	if(temp==nil)then
		item_forbid_flag[i+1] = ""
	else
		item_forbid_flag[i+1] = xml.get_value("create_flag")
	end
	xml.pop()
end

--generations: faction
local captain_name = ai.generate_name("Terrestrial", "male")
if(faction_name==nil)then
	faction_name = "Crew of "..captain_name
end
ai.create_faction(faction_name, faction_type)
local total_guards = math.random(min_guards, max_guards)
--generations: guards
ship.load_self()
local size_x = object.get_size_x()
if(total_guards>0)then
	if(#guard_probabilities==0)then
		print(object.get_name().." has no defined guards")
	end
	local guard_added_probabilities = {}
	guard_added_probabilities[1] = 0
	for i=1,#guard_added_probabilities do
		guard_added_probabilities[i+1] = guard_added_probabilities[i]+guard_probabilities[i]
	end
	for i=1,#guard_added_probabilities do
		guard_added_probabilities[i] = guard_added_probabilities[i]/guard_added_probabilities[#guard_added_probabilities]
	end
	while(total_guards>0)do
		--select a guard
		local rnd = math.random()
		local choice = guard_types[1]
		for i=1,#guard_types do
			if(rnd<=guard_added_probabilities[i])then
				choice = guard_types[i]
				break
			end
		end
		--spawn the guard
		local x = -size_x/2+math.random()*size_x
		object.create_character(faction_name, x, 0, choice)
		total_guards = total_guards-1
	end
end

local captain_added_probabilities = {}
captain_added_probabilities[1] = 0
for i=1,#captain_added_probabilities do
	captain_added_probabilities[i+1] = captain_added_probabilities[i]+captain_probabilities[i]
end
for i=1,#captain_added_probabilities do
	captain_added_probabilities[i] = captain_added_probabilities[i]/captain_added_probabilities[#captain_added_probabilities]
end
--select a captain
if(#captain_types==0)then
	print(object.get_name().." has no defined captain")
end
local rnd = math.random()
local choice = captain_types[1]
for i=1,#captain_types do
	if(rnd<=captain_added_probabilities[i])then
		choice = captain_types[i]
		break
	end
end
--spawn the captain
object.create_character(faction_name, ship.get_control_x(), ship.get_control_y(), choice)
--make captain control ship
ai.load_self()
--TODO: decide if captains should be completely looted or not by un-commenting or not the following line
--object.set_alias(captain_name)
ai.control_parent()
--create items
local slots = ship.get_number_of_items()
local flags = {}
for i=1,#item_types do
	local has_flag = false
	if(item_require_flag[i]=="")then
		has_flag = true
	else
		for j=1,#flags do
			if(flags[j]==item_require_flag[i])then
				has_flag = true
				break
			end
		end
	end
	local has_not_flag = false
	if(item_forbid_flag[i]=="")then
		has_not_flag = false
	else
		for j=1,#flags do
			if(flags[j]==item_forbid_flag[i])then
				has_not_flag = true
				break
			end
		end
	end
	if(has_flag)and(has_not_flag==false)and(math.random()<=item_probabilities[i])then
		for slot=0,slots-1 do
			if(ship.load_item(slot)==false)and(ship.create_item(item_types[i], slot, item_quantity[i]))then
				if(item_create_flag[i]~="")then
					flags[#flags+1] = item_create_flag[i]
				end
				break
			end			
		end
	end
end
--create bounty
if(bounty>0)then
	ai.add_bounty(ai.get_faction(), bounty)
end