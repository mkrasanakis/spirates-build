--characteristics: general
local size_x = object.get_size_x()
local name = object.get_name()
xml.load_node(name, "hull", "name")
--characteristics: buildings
local number_of_buildings = xml.count("building")
--remove buildings
for i=0,number_of_buildings-1 do
	xml.push()
	xml.load_node("building", i)
	local name = xml.get_value("name")
	object.remove(name)
	xml.pop()
end
--add buildings
for i=0,number_of_buildings-1 do
	xml.push()
	xml.load_node("building", i)
	local name = xml.get_value("name")
	local x = xml.get_value("x")
	if(x==nil)then
		x = 0
	else
		x = tonumber(x)
	end
	local y = xml.get_value("y")
	if(y==nil)then
		y = 0
	else
		y = tonumber(y)
	end
	object.create_building(x, y, name)
	xml.pop()
end